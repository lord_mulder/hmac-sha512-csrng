@echo off

if not exist "%JAVA_HOME%\bin\java.exe" (
	echo Java could not be found. Please check your JAVA_HOME and try again!
	pause
	goto:eof
)

call "%~dp0..\Prerequisites\Ant\bin\ant.bat"
pause
