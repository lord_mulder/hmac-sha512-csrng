CSRNG (Cryptographically secure random number generator) based on HMAC-SHA512 algorithm
Created by LoRd_MuldeR <mulder2@gmx.de>.
Please visit the official web-site at http://muldersoft.com/ for news and updates!

This work is licensed under the CC0 1.0 Universal License.
To view a copy of the license, visit:
https://creativecommons.org/publicdomain/zero/1.0/legalcode

The HmacSha512Csrng class has been designed to work with Java 8, or any later Java version.
Please note that this class is NOT thread-safe!
You can use the static method 'getInstance()' to obtain a separate instance for each thread.

EXAMPLE:

	import com.muldersoft.hmacsha512csrng.HmacSha512Csrng;
	
	public static void main(String[] args) {
		final HmacSha512Csrng hmacSha512Csrng = HmacSha512Csrng.getInstance();
		for(int i = 0; i < 42; ++i) {
			System.out.printf("%016X\n", hmacSha512Csrng.nextLong(true));
		}
	}
