/*
 * CSRNG (Cryptographically secure random number generator) based on HMAC-SHA512 algorithm
 * Created by LoRd_MuldeR <mulder2@gmx.de>.
 * Please visit the official web-site at http://muldersoft.com/ for news and updates!
 * 
 * This work is licensed under the CC0 1.0 Universal License.
 * To view a copy of the license, visit:
 * https://creativecommons.org/publicdomain/zero/1.0/legalcode
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Encoder;

import org.apache.commons.math3.random.MersenneTwister;

public class GenerateTables {

	private static final int ROUNDS = 99999;
	private static final int ROUNDS_SQRT = (int) Math.ceil(Math.sqrt(ROUNDS));

	private static final int MIN_DISTANCE = 240;

	private static final byte[] SEED = Base64.getDecoder().decode("LZWn5cM6UHegaWwszdYnsRfLe933x7ZoqwyJydXOayzbBKVOmu0P7/q7Ll/I5CvRX124yoHnJxJ5HgPQCIdxZZeIjx69Hfm1pBSQ3aeKNrjek+1LAV/DnQFdbXYiez77alPeelCzgQOG+W/j0A5fre49NtKvpE8YUjdECysO39Ldb0UIGd6TbTvZOkGWjO+FjsPvG0Oj0Kf801sZTVnwqD0cKFl9FMzTMSXjF12pTEVEp9/Gdd+d+29JvpH0cCDRhXHczGdrYmIeuzL/k7zdBm4RyymgDcRazL6DmffQcgdvn6xfNPwhQBbHSMiY1fYxnSjsLHiWhpDgiyCA/ubSYufAMqsumDxXVJjZVhZwMr/ubLYGHH31pmS01oabXlsVKlC3jPUh2UdHtwO4L3GsDT0mPUI1ND/IRQaZZLsXtQMyrsnDTaCl+gzUugvDyy04d9G7wuDrAlpACkmgLrP+AxMzv4yzKMIaGD8WWg48TgQViha5XkL0ukRVJLUdAGZAVJh+kJVG115bErOj7VF4tUCnmjUdulx2Rg35bRJLNBv4HGslCQMOZ8C9ztSy7O8sXKfyKgtTQ7VD70mZjxO0EwQyeWh7XLTH8IAjX2ZFO8gpLwFeBrv2pOZuClpYeP2lF1ptBUiG6ynsE/4PefUBswTi1A5x9XdRRGk1D6avQJCo1/+lqyE9iPEEVoZAFjNGKSo95oj76dIFoxZiNuf24FxSCyK2/X+FBzB70Bh2LFJuIg47+NGmdsFq2rhitxdgVkldrZEYX0OctaQ0uuZqHYThr9JeMa0GRp9JRa59ohfuEcE7ZznLuB2S8s0RUwvR");
	
	public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
		// Initialize tables
		final long[][][] table = new long[2][][];
		for (int t = 0; t < 2; ++t) {
			table[t] = new long[256][];
			for (int i = 0; i < 256; ++i) {
				table[t][i] = new long[8];
			}
		}

		// Temporary rows
		final long[] next = new long[8];
		final long[] best = new long[8];

		// Initialize MT-RNG
		final MersenneTwister random = new MersenneTwister(bytesToIntegers(SEED));
		
		// Generate tables!
		for (int t = 0; t < 2; ++t) {
			for (int i = 0; i < 256; ++i) {
				System.out.printf("Generating table %d of %d, row %d of %d..", t + 1, 2, i + 1, 256);
				int bestDistance = Integer.MIN_VALUE;
				optimize: do {
					boolean replacedFlag = false;
					System.out.print(".");
					/* ------------------------------------------------------- */
					/* Whole string replacement */
					/* ------------------------------------------------------- */
					for (int r = 0; r < ROUNDS; ++r) {
						for (int j = 0; j < 8; ++j) {
							next[j] = random.nextLong();
						}
						final int nextDistance = checkTable(table, t, i, next);
						if (nextDistance > bestDistance) {
							System.out.print("*");
							System.arraycopy(next, 0, best, 0, next.length);
							bestDistance = nextDistance;
							replacedFlag = true;
							if (bestDistance >= MIN_DISTANCE) {
								break optimize;
							}
						}
					}
					/* ------------------------------------------------------- */
					/* Long word replacement */
					/* ------------------------------------------------------- */
					for (int r = 0; r < ROUNDS_SQRT; ++r) {
						for (int j = 0; j < 8; ++j) {
							System.arraycopy(best, 0, next, 0, best.length);
							for (int q = 0; q < ROUNDS_SQRT; q++) {
								next[j] = random.nextLong();
								final int nextDistance = checkTable(table, t, i, next);
								if (nextDistance > bestDistance) {
									System.out.printf("L%d", j);
									System.arraycopy(next, 0, best, 0, next.length);
									bestDistance = nextDistance;
									replacedFlag = true;
									if (bestDistance >= MIN_DISTANCE) {
										break optimize;
									}
								}
							}
						}
					}
					/* ------------------------------------------------------- */
					/* Integer word replacement */
					/* ------------------------------------------------------- */
					for (int r = 0; r < ROUNDS_SQRT; ++r) {
						for (int j = 0; j < 8; ++j) {
							System.arraycopy(best, 0, next, 0, best.length);
							for (int q = 0; q < ROUNDS_SQRT; q++) {
								final int z = random.nextInt(33);
								next[j] = best[j] ^ (Integer.toUnsignedLong(random.nextInt()) << z);
								final int nextDistance = checkTable(table, t, i, next);
								if (nextDistance > bestDistance) {
									System.out.printf("I%d%02d", j, z);
									System.arraycopy(next, 0, best, 0, next.length);
									bestDistance = nextDistance;
									replacedFlag = true;
									if (bestDistance >= MIN_DISTANCE) {
										break optimize;
									}
								}
							}
						}
					}
					/* ------------------------------------------------------- */
					/* Single byte replacement */
					/* ------------------------------------------------------- */
					if (replacedFlag) {
						for (;;) {
							boolean improved = false;
							for (int j = 0; j < 8; ++j) {
								System.arraycopy(best, 0, next, 0, best.length);
								for (int k = 0; k < 64; k += 8) {
									for (int q = 1; q < 256; q++) {
										next[j] = best[j] ^ (Integer.toUnsignedLong(q) << k);
										final int nextDistance = checkTable(table, t, i, next);
										if (nextDistance > bestDistance) {
											System.out.printf("B%d%02d", j, k);
											System.arraycopy(next, 0, best, 0, next.length);
											bestDistance = nextDistance;
											improved = true;
											if (bestDistance >= MIN_DISTANCE) {
												break optimize;
											}
										}
									}
								}
							}
							if (!improved) {
								break;
							}
						}
					}
				} while (bestDistance < MIN_DISTANCE);
				System.arraycopy(best, 0, table[t][i], 0, best.length);
				System.out.println(" done.");
			}
		}

		// Completed
		System.out.println("Completed.\n");

		// Validate table
		if (!validateTable(table)) {
			throw new RuntimeException("Table validation has failed !!!");
		}

		// Print table
		for (int t = 0; t < 2; ++t) {
			for (int i = 0; i < 256; ++i) {
				final Encoder encoder = Base64.getEncoder();
				try (final ByteArrayOutputStream lineBuffer = new ByteArrayOutputStream()) {
					for (int j = 0; j < 8; ++j) {
						lineBuffer.write(longToBytes(table[t][i][j]));
					}
					System.out.printf("decoder.decode(\"%s\"), /*%02X*/\n", encoder.encodeToString(lineBuffer.toByteArray()), i);
				}
			}
			System.out.println("------------------------------------------------------");
		}
	}

	private static int checkTable(final long[][][] table, final int tabIdx, final int rowIdx, final long[] candidate) {
		int minDistance = Integer.MAX_VALUE;
		for (int i = 0; i < rowIdx; ++i) {
			minDistance = Math.min(minDistance, rowDistance(table[tabIdx][i], candidate));
		}
		for (int t = 0; t < tabIdx; ++t) {
			minDistance = Math.min(minDistance, rowDistance(table[t][rowIdx], candidate));
		}
		return minDistance;
	}

	private static boolean validateTable(final long[][][] table) {
		for (int t = 0; t < 2; ++t) {
			for (int i = 0; i < 256; ++i) {
				for (int j = 0; j < 256; ++j) {
					if (i != j) {
						if (rowDistance(table[t][i], table[t][j]) < MIN_DISTANCE) {
							return false;
						}
					}
				}
			}
		}
		for (int t = 0; t < 2; ++t) {
			for (int u = 0; u < 2; ++u) {
				if(t != u) {
					for (int i = 0; i < 256; ++i) {
						if (rowDistance(table[t][i], table[u][i]) < MIN_DISTANCE) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private static int rowDistance(final long[] row1, final long[] row2) {
		int distance = 0;
		for (int j = 0; j < 8; ++j) {
			distance += hammingDistance(row1[j], row2[j]);
		}
		return distance;
	}

	private static int hammingDistance(final long value1, final long value2) {
		int dist = 0;
		long temp = value1 ^ value2;
		while (temp != 0) {
			++dist;
			temp &= temp - 1;
		}
		return dist;
	}
	
	private static byte[] longToBytes(final long value) {
		final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		buffer.putLong(value);
		return buffer.array();
	}

	private static int[] bytesToIntegers(byte[] bytes) {
		final int[] integers = new int[bytes.length / Integer.BYTES];
		final ByteBuffer buffer = ByteBuffer.wrap(bytes);
		for(int i = 0; i < integers.length; ++i) {
			integers[i] = buffer.getInt();
		}
		return integers;
	}
}
