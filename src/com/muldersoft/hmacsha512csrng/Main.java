/*
 * CSRNG (Cryptographically secure random number generator) based on HMAC-SHA512 algorithm
 * Created by LoRd_MuldeR <mulder2@gmx.de>.
 * Please visit the official web-site at http://muldersoft.com/ for news and updates!
 * 
 * This work is licensed under the CC0 1.0 Universal License.
 * To view a copy of the license, visit:
 * https://creativecommons.org/publicdomain/zero/1.0/legalcode
 */

package com.muldersoft.hmacsha512csrng;

import java.util.Locale;

/**
 * A simple command-line front-end that shows how to use the HmacSha512Csrng class
 */
public class Main {

	/**
	 * Size of the buffer
	 */
	private final static int BUFFER_SIZE = 4096;
	
	/**
	 * Hex characters
	 */
	private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
	
	/**
	 * Mode of operation
	 */
	private enum ModeOfOperation {
		BYTES,
		INTEGERS,
		LONGS
	}
	
	/**
	 * Main method (entry point)
	 */
	public static void main(final String[] args) {
		// Check arguments
		if((args.length < 1) || ((args.length > 0) && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("--version")))) {
			printLogo();
			System.err.println("Usage:");
			System.err.println("  java -jar hmacsha512csrng.jar [--int|--long|--hex] <count>\n");
			System.err.println("Default generates \"raw\" bytes. Use '--int' or '--long' to generate numbers."); 
			System.err.println("Use option '--hex' to enable hexadecimal formatted output."); 
			System.err.println("Count must be non-negative. Specify as '-' to generate an unlimited stream.");
			System.exit(1);
		}

		// Set up defaults
		ModeOfOperation mode = ModeOfOperation.BYTES;
		boolean printAsHex = false;

		// Parse options
		if(args.length > 1) {
			for(int i = 0; i < args.length - 1; ++i) {
				final String userOption = args[i].toLowerCase(Locale.US);
				switch(userOption) {
				case "--int":
					if(mode == ModeOfOperation.LONGS) {
						System.err.println("Error: Options \"--int\" and \"--long\" are mutually exclusive!");
						System.exit(1);
					}
					mode = ModeOfOperation.INTEGERS;
					break;
				case "--long":
					if(mode == ModeOfOperation.INTEGERS) {
						System.err.println("Error: Options \"--long\" and \"--int\" are mutually exclusive!");
						System.exit(1);
					}
					mode = ModeOfOperation.LONGS;
					break;
				case "--hex":
					printAsHex = true;
					break;
				default:
					printLogo();
					System.err.println("Error: Option \"" + userOption + "\" is unsupported!");
					System.exit(1);
				}
			}
		}
		
		// Try to parse the given count
		long count = -1L;
		final String countString = args[args.length-1];
		try {
			if(!countString.equals("-")) {
				final long multiplier = parseCountSuffix(countString);
				final long value = Long.parseLong((multiplier > 1L) ? countString.substring(0, countString.length() - 1) : countString);
				if(value < 0) {
					System.err.println("Error: The specified count must not be a negative value!");
					System.exit(1);
				} else {
					count = value * multiplier;
				}
			}
		} catch(final NumberFormatException e) {
			printLogo();
			System.err.println("Error: The specified count \"" + countString + "\" is invalid!");
			System.exit(1);
		}
		
		// Generate output
		generateOutput(mode, printAsHex, count);
	}

	/**
	 * Generate random output
	 */
	private static void generateOutput(final ModeOfOperation mode, final boolean printAsHex, final long count) {
		try {
			final HmacSha512Csrng hmacSha512Csrng = HmacSha512Csrng.getInstance();
			switch(mode) {
			case BYTES:
				generateByteOutput(hmacSha512Csrng, printAsHex, count);
				break;
			case INTEGERS:
				generateNumericOutput(hmacSha512Csrng, false, printAsHex, count);
				break;
			case LONGS:
				generateNumericOutput(hmacSha512Csrng, true, printAsHex, count);
				break;
			default:
				throw new RuntimeException("Unknown mode of operation encountered!");
			}
		} catch(final Exception e) {
			System.err.println("Whoops, an error was encountered:");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Generate random bytes as output
	 */
	private static void generateByteOutput(final HmacSha512Csrng hmacSha512Csrng, final boolean printAsHex, final long count) {
		final byte[] buffer = new byte[BUFFER_SIZE];
		if(count >= 0) {
			int batchSize;
			for(long i = 0; i < count; i += batchSize) {
				batchSize = (int) Math.min(count - i, BUFFER_SIZE);
				hmacSha512Csrng.nextBytes(buffer, 0, batchSize);
				if(!printAsHex) {
					System.out.write(buffer, 0, batchSize);
				} else {
					System.out.print(bytesToHex(buffer, batchSize));
				}
				if(System.out.checkError()) {
					System.err.println("Error: Write operation has failed!");
					break;
				}
			}
		} else {
			for(;;) {
				hmacSha512Csrng.nextBytes(buffer, 0, BUFFER_SIZE);
				if(!printAsHex) {
					System.out.write(buffer, 0, BUFFER_SIZE);
				} else {
					System.out.print(bytesToHex(buffer, BUFFER_SIZE));
				}
				if(System.out.checkError()) {
					break; /*expected to happen for unbound stream*/
				}
			}
		}
	}

	/**
	 * Generate random numbers as output
	 */
	private static void generateNumericOutput(final HmacSha512Csrng hmacSha512Csrng, final boolean longs, final boolean printAsHex, final long count) {
		if(count >= 0) {
			for(long i = 0; i < count; ++i) {
				if(!longs) {
					System.out.printf(printAsHex ? "0x%08X\n"  : "%d\n", hmacSha512Csrng.nextInt (printAsHex));
				} else {
					System.out.printf(printAsHex ? "0x%016X\n" : "%d\n", hmacSha512Csrng.nextLong(printAsHex));
				}
				if(System.out.checkError()) {
					System.err.println("Error: Write operation has failed!");
					break;
				}
			}
		} else {
			for(;;) {
				if(!longs) {
					System.out.printf(printAsHex ? "0x%08X\n"  : "%d\n", hmacSha512Csrng.nextInt (printAsHex));
				} else {
					System.out.printf(printAsHex ? "0x%016X\n" : "%d\n", hmacSha512Csrng.nextLong(printAsHex));
				}
				if(System.out.checkError()) {
					break; /*expected to happen for unbound stream*/
				}
			}
		}
	}

	/**
	 * Convert bytes to hex characters
	 */
	private static char[] bytesToHex(final byte[] buffer, final int length) {
		final char[] hexChars = new char[length * 2];
		for (int j = 0, k = 0; j < length; j++, k += 2) {
			final int value = buffer[j] & 0xFF;
			hexChars[k] = HEX_ARRAY[value >>> 4];
			hexChars[k + 1] = HEX_ARRAY[value & 0x0F];
		}
		return hexChars;
	}
	
	/**
	 * Parse the suffix of count specification
	 */
	private static long parseCountSuffix(final String str) {
		if(str.length() > 0) {
			switch(Character.toLowerCase(str.charAt(str.length() - 1))) {
			case 'k':
				return 1024L;
			case 'm':
				return 1048576L;
			case 'g':
				return 1073741824L;
			case 't':
				return 1099511627776L;
			}
		}
		return 1L;
	}
	
	/**
	 * Print application name and license info
	 */
	private static void printLogo() {
		System.err.printf("HMAC-SHA512-based CSRNG v%d.%02d, created by LoRd_MuldeR <mulder2@gmx.de>.\n",
				HmacSha512Csrng.VERSION_MAJOR, HmacSha512Csrng.VERSION_MINOR);
		System.err.println("This work is licensed under the CC0 1.0 Universal License.\n");
	}
}
