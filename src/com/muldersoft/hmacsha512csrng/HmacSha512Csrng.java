/*
 * CSRNG (Cryptographically secure random number generator) based on HMAC-SHA512 algorithm
 * Created by LoRd_MuldeR <mulder2@gmx.de>.
 * Please visit the official web-site at http://muldersoft.com/ for news and updates!
 * 
 * This work is licensed under the CC0 1.0 Universal License.
 * To view a copy of the license, visit:
 * https://creativecommons.org/publicdomain/zero/1.0/legalcode
 */

package com.muldersoft.hmacsha512csrng;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.text.DateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.security.auth.DestroyFailedException;

/**
 * CSRNG (Cryptographically secure random number generator) based on HMAC-SHA512 algorithm<br>
 * Please note that this class is <b>not</b> thread-safe!</br>
 * You can use the <tt>getInstance()</tt> method to obtain a separate instance for each thread.
 */
public class HmacSha512Csrng {

	/**
	 * Version information
	 */
	public static final int VERSION_MAJOR = 1;
	public static final int VERSION_MINOR = 0;

	/**
	 * HMAC-SHA512 identifier
	 */
	private static final String HMAC_SHA512 = "HmacSHA512";

	/**
	 * SHA-512 size in bytes
	 */
	private static final int SHA512_SIZE = 64;
	
	/**
	 * File name to persist entropy
	 */
	private static final String ENTROPY_FILE_NAME = ".hmacsha512csrng-1.seed";

	/**
	 * Force re-seed every N blocks
	 */
	private static final int RESEED_BLOCKS = 4093;

	/**
	 * Timeout for cached random data
	 */
	private static final int RANDOM_TIMEOUT = 29989;

	/**
	 * Unique instance identifier<br>
	 * The initial value is kind of arbitrary and was chosen from truly random data. It is recommended to replace this value with another truly random value (e.g. from random.org), if you use this class in your application!
	 */
	private static final AtomicLong instanceUniqueId = new AtomicLong(0xF1784B0F72D7C560L);

	/**
	 * Fixed byte array used in the initialization process<br>
	 * This data is kind of arbitrary and was chosen from truly random data. It is recommended to replace this data with other truly random data (e.g. from random.org), if you use this class in your application!
	 */
	private static final byte[] INITIALIZER = Base64.getDecoder().decode("+aQEbyIJcRJnlDkT4W7Dr5q48EXrRCIcZE6+EUK4PXWoqx27PWs8IOH5pElvtAniZvJooh4ztZXXRWZ7Tz0VQUInRH+92LlXakDE6lJaPoHoK4Fey9Y62k6GpqJ2k3rN1f9q6a6591UjNEr3oqGoOjfGfJ5AW9KEIQMOjoNVqO0=");
	
	/**
	 * The <i>thread-local</i> CSRNG instance
	 */
	private static final ThreadLocal<HmacSha512Csrng> threadLocal = ThreadLocal.withInitial(() -> new HmacSha512Csrng());

	/**
	 * The HMAC-SHA512 instance of <i>this</i> CSRNG instance
	 */
	private final Mac hmacSha512;

	/**
	 * Automatic re-seed flag; set to <tt>true</tt> if automatic re-seeding is enabled
	 */
	private final boolean autoReseed;

	/**
	 * Time at which next re-seed will be triggered
	 */
	private long nextReseedTime = System.currentTimeMillis() + RANDOM_TIMEOUT;

	/**
	 * Counter for block generation<br>
	 * The initial value is kind of arbitrary, but <tt>initial_counter % RESEED_BLOCKS == 1</tt> <b>should</b> hold true. It is recommended to replace this value, if you use this class in your application!
	 */
	private long counter = 0x6C18EEDBFBA547C9L;

	/* =================================================================== */
	/* STATIC INITIALIZATION                                               */
	/* =================================================================== */

	/**
	 * Holder class for the logging handler
	 */
	private static class LoggerHolder {
		static final Logger LOGGER;
		static {
			try {
				LOGGER = Logger.getLogger(HmacSha512Csrng.class.getName());
			} catch(final Exception e) {
				throw new Error("Logger could not be created!", e);
			}
		}
	}

	/**
	 * Holder class for the global <i>static</i> seed value
	 */
	private static class StaticSeedHolder {
		static final byte[] STATIC_SEED;
		static {
			final Mac hmacSha512;
			try {
				hmacSha512 = Mac.getInstance(HMAC_SHA512);
				initialize(hmacSha512, loadEntropyData());
			} catch(final Exception e) {
				throw new Error("HMAC-SHA512 instance could not be created!", e);
			}
			try(final CompressedOutputStream entropy = new CompressedOutputStream()) {
				entropy.write(querySystemTimers());
				entropy.write(System.identityHashCode(HmacSha512Csrng.class));
				entropy.write(timeApproximationOfPi());
				entropy.write(getProcessTimers());
				entropy.write(getPhysicalMemoryInfo());
				entropy.write(getProcessCpuTime());
				entropy.write(getThreadCpuUsageInfo());
				entropy.write(getClassLoaderInfo());
				entropy.write(getCurrentDirectory());
				entropy.write(getDateTimeString());
				entropy.write(getProcessMemoryInfo());
				entropy.write(getJavaRuntimeInfo());
				entropy.write(getOperatingSystemInfo());
				entropy.write(getCurrentUserInfo());
				entropy.write(getGarbageCollectorInfo());
				entropy.write(getLocalNetworkInfo());
				entropy.write(querySystemTimers());
				STATIC_SEED = generateKey(hmacSha512, entropy.toByteArray());
				saveEntropyData(generateKey(hmacSha512, STATIC_SEED));
			} catch(final Exception e) {
				throw new Error("Failed to generate the static seed value!", e);
			}
		}
	}
	
	/**
	 * Holder class for <i>optional</i> classes and methods (resolved by reflection)
	 */
	private static class OptionalMethodsHolder {
		static final Class<?> OPERATING_SYSTEM_MX_BEAN = tryResolveClassByName("com.sun.management.OperatingSystemMXBean");
		static final Class<?> NT_SYSTEM = tryResolveClassByName("com.sun.security.auth.module.NTSystem");
		static final Class<?> UNIX_SYSTEM = tryResolveClassByName("com.sun.security.auth.module.UnixSystem");
		static final Method GET_PROCESS_CPU_TIME = tryResolveMethodByName(OPERATING_SYSTEM_MX_BEAN, "getProcessCpuTime");
		static final Method GET_TOTAL_PHYSICAL_MEMORY = tryResolveMethodByName(OPERATING_SYSTEM_MX_BEAN, "getTotalPhysicalMemorySize");
		static final Method GET_FREE_PHYSICAL_MEMORY = tryResolveMethodByName(OPERATING_SYSTEM_MX_BEAN, "getFreePhysicalMemorySize");
		static final Method GET_COMMITTED_VIRTUAL_MEMORY = tryResolveMethodByName(OPERATING_SYSTEM_MX_BEAN, "getCommittedVirtualMemorySize");
		static final Method GET_NT_USER_NAME = tryResolveMethodByName(NT_SYSTEM, "getName");
		static final Method GET_NT_USER_ID = tryResolveMethodByName(NT_SYSTEM, "getUserSID");
		static final Method GET_UNIX_USER_NAME = tryResolveMethodByName(UNIX_SYSTEM, "getUsername");
		static final Method GET_UNIX_USER_ID = tryResolveMethodByName(UNIX_SYSTEM, "getUid");
	}
	
	/* =================================================================== */
	/* CONSTRUCTOR                                                         */
	/* =================================================================== */

	/**
	 * Initialize with a random seed; automatic periodic re-seeding is enabled
	 */
	public HmacSha512Csrng() {
		this(true);
	}
	
	/**
	 * Initialize with a random seed; automatic periodic re-seeding is optional
	 */
	public HmacSha512Csrng(final boolean autoReseed) {
		this.autoReseed = autoReseed;
		try {
			hmacSha512 = Mac.getInstance(HMAC_SHA512);
			initialize(hmacSha512, StaticSeedHolder.STATIC_SEED.clone());
		} catch(Exception e) {
			throw new Error("HMAC-SHA512 instance could not be created!", e);
		}
		collectAdditionalEntropy(false);
	}

	/**
	 * Initialize with a user-provided seed; automatic periodic re-seeding is disabled
	 */
	public HmacSha512Csrng(final long seed) {
		this(seed, false);
	}

	/**
	 * Initialize with a user-provided seed; automatic periodic re-seeding is optional
	 */
	public HmacSha512Csrng(final long seed, final boolean autoReseed) {
		this(longToBytes(seed), autoReseed);
	}
	
	/**
	 * Initialize with a user-provided seed; automatic periodic re-seeding is disabled
	 */
	public HmacSha512Csrng(final String seed) {
		this(seed, false);
	}

	/**
	 * Initialize with a user-provided seed; automatic periodic re-seeding is optional
	 */
	public HmacSha512Csrng(final String seed, final boolean autoReseed) {
		this(stringToBytes(seed), autoReseed);
	}
	
	/**
	 * Initialize with a user-provided seed. Automatic periodic re-seeding is disabled
	 */
	public HmacSha512Csrng(final byte[] seed) {
		this(seed, false);
	}

	/**
	 * Initialize with a user-provided seed. Automatic periodic re-seeding is optional
	 */
	public HmacSha512Csrng(final byte[] seed, final boolean autoReseed) {
		if(isNullOrEmpty(seed)) {
			throw new IllegalArgumentException("Seed must not be null or empty!");
		}
		this.autoReseed = autoReseed;
		try {
			hmacSha512 = Mac.getInstance(HMAC_SHA512);
			initialize(hmacSha512, autoReseed ? StaticSeedHolder.STATIC_SEED.clone() : INITIALIZER.clone());
		} catch(Exception e) {
			throw new Error("HMAC-SHA512 instance could not be created!", e);
		}
		for(int i = 0; i < 9973; ++i) {
			addEntropy(seed);
		}
	}
	
	/* =================================================================== */
	/* PUBLIC METHODS                                                      */
	/* =================================================================== */
	
	/**
	 * Return one random byte<br>
	 * <b>Warning:</b> This method is very inefficient to generate <i>multiple</i> bytes!
	 */
	public byte nextByte() {
		final ByteBuffer generatedBlock = generateBlocks(1);
		return generatedBlock.get();
	}

	/**
	 * Fills the given buffer entirely with random bytes
	 */
	public void nextBytes(final byte[] outputBuffer) {
		if(isNullOrEmpty(outputBuffer)) {
			throw new IllegalArgumentException("Output buffer must not be null or empty!");
		}
		nextBytes(outputBuffer, 0, outputBuffer.length);
	}
	
	/**
	 * Writes the specified number of random bytes to the given buffer, from the given offset
	 */
	public void nextBytes(final byte[] outputBuffer, final int outputOffset, final int outputLength) {
		if(outputLength < 1) {
			throw new IllegalArgumentException("Output length count must be a positive value!");
		}
		if(isNullOrEmpty(outputBuffer)) {
			throw new IllegalArgumentException("Output buffer must not be null or empty!");
		}
		if(Math.addExact(outputOffset, outputLength) > outputBuffer.length) {
			throw new IllegalArgumentException("Output offset plus output length must not exceed output buffer size!");
		}
		final ByteBuffer generatedBlocks = generateBlocks(divideRoundUp(outputLength, SHA512_SIZE));
		generatedBlocks.get(outputBuffer, outputOffset, outputLength);
	}
	
	/**
	 * Returns the specified number of random bytes, in a newly allocated buffer
	 */
	public byte[] nextBytes(final int outputLength) {
		if(outputLength < 1) {
			throw new IllegalArgumentException("Output length count must be a positive value!");
		}
		final ByteBuffer generatedBlocks = generateBlocks(divideRoundUp(outputLength, SHA512_SIZE));
		if(generatedBlocks.capacity() > outputLength) {
			final byte[] outputBuffer = new byte[outputLength];
			generatedBlocks.get(outputBuffer, 0, outputLength);
			return outputBuffer;
		} else {
			return generatedBlocks.array(); /*exact multiple of block size*/
		}
	}

	/* ------------------------------------------------------------------- */
	/* CONVENIENCE WRAPPERS                                                */
	/* ------------------------------------------------------------------- */

	/**
	 * Returns an unsigned random integer<br>
	 * The result will be a positive (unsigned) value, equally distributed in the [0,Integer.MAX_VALUE] range.<br>
	 * <b>Warning:</b> This method is very inefficient to generate <i>multiple</i> integer values!
	 */
	public int nextInt() {
		return nextInt(false);
	}
	
	/**
	 * Returns a random integer, optionally signed<br>
	 * If signed, the result will be equally distributed in the [Integer.MIN_VALUE,Integer.MAX_VALUE] range; otherwise the result will be equally distributed in the [0,Integer.MAX_VALUE] range.
	 */
	public int nextInt(final boolean signed) {
		final int value =  generateBlocks(1).getInt();
		return signed ? value : (value >>> 1);
	}

	/**
	 * Returns an unsigned random integer with user-defined maximum<br>
	 * The result will be equally distributed in the [0,bound) range, i.e. it is always non-negative and less than bound.
	 */
	public int nextInt(final int bound) {
		if (bound < 1) {
			throw new IllegalArgumentException("Bound must be a positive value!");
		}
		final int upperLimit = Integer.divideUnsigned(Integer.MAX_VALUE + 1, bound) * bound;
		if(upperLimit > 0) {
			int value;
			ByteBuffer currentBlock = generateBlocks(1);
			do {
				if(currentBlock.remaining() < Integer.BYTES) {
					currentBlock = generateBlocks(1);
				}
				value = (currentBlock.getInt() >>> 1);
			} while(value >= upperLimit);
			return value % bound;
		} else {
			return nextInt(false) % bound; /*the trivial case*/
		}
	}

	/**
	 * Returns an array of random unsigned integers<br>
	 * If signed, each value will be equally distributed in the [Integer.MIN_VALUE,Integer.MAX_VALUE] range; otherwise the result will be equally distributed in the [0,Integer.MAX_VALUE] range.
	 */
	public int[] nextInts(final int outputLength) {
		if (outputLength < 1) {
			throw new IllegalArgumentException("Count must be a positive value!");
		}
		final int[] outputBuffer = new int[outputLength];
		final IntBuffer values = generateBlocks(divideRoundUp(Math.multiplyExact(outputLength, Integer.BYTES), SHA512_SIZE)).asIntBuffer();
		for(int i = 0; i < outputLength; ++i) {
			outputBuffer[i] = values.get();
		}
		return outputBuffer;
	}
	
	/**
	 * Returns an unsigned random long<br>
	 * The result will be a positive (unsigned) value, equally distributed in the [0L,Long.MAX_VALUE] range.<br>
	 * <b>Warning:</b> This method is very inefficient to generate <i>multiple</i> integer values!
	 */
	public long nextLong() {
		return nextLong(false);
	}
	
	/**
	 * Returns a random long, optionally signed<br>
	 * If signed, the result will be equally distributed in the [Long.MIN_VALUE,Long.MAX_VALUE] range; otherwise the result will be equally distributed in the [0L,Long.MAX_VALUE] range.
	 */
	public long nextLong(final boolean signed) {
		final long value = generateBlocks(1).getLong();
		return signed ? value : (value >>> 1);
	}

	/**
	 * Returns an unsigned random long with user-defined maximum<br>
	 * The result will be equally distributed in the [0L,bound) range, i.e. it is always non-negative and less than bound.
	 */
	public long nextLong(final long bound) {
		if (bound < 1) {
			throw new IllegalArgumentException("Bound must be a positive value!");
		}
		final long upperLimit = Long.divideUnsigned(Long.MAX_VALUE + 1L, bound) * bound;
		if(upperLimit > 0) {
			long value;
			ByteBuffer currentBlock = generateBlocks(1);
			do {
				if(currentBlock.remaining() < Long.BYTES) {
					currentBlock = generateBlocks(1);
				}
				value = (currentBlock.getLong() >>> 1);
			} while(value >= upperLimit);
			return value % bound;
		} else {
			return nextLong(false) % bound; /*the trivial case*/
		}
	}

	/**
	 * Returns an array of random unsigned integers<br>
	 * If signed, each value will be equally distributed in the [Integer.MIN_VALUE,Integer.MAX_VALUE] range; otherwise the result will be equally distributed in the [0,Integer.MAX_VALUE] range.
	 */
	public long[] nextLongs(final int outputLength) {
		if (outputLength < 1) {
			throw new IllegalArgumentException("Count must be a positive value!");
		}
		final long[] outputBuffer = new long[outputLength];
		final LongBuffer values = generateBlocks(divideRoundUp(Math.multiplyExact(outputLength, Long.BYTES), SHA512_SIZE)).asLongBuffer();
		for(int i = 0; i < outputLength; ++i) {
			outputBuffer[i] = values.get();
		}
		return outputBuffer;
	}
	
	/* ------------------------------------------------------------------- */
	/* ADD ENTROPY TO STATE                                                */
	/* ------------------------------------------------------------------- */

	/**
	 * Add some additional entropy to the internal state of the CSRNG
	 */
	public void addEntropy(final int entropy) {
		addEntropy(intToBytes(entropy));
	}
	
	/**
	 * Add some additional entropy to the internal state of the CSRNG
	 */
	public void addEntropy(final long entropy) {
		addEntropy(longToBytes(entropy));
	}

	/**
	 * Add some additional entropy to the internal state of the CSRNG
	 */
	public void addEntropy(final String entropy) {
		addEntropy(stringToBytes(entropy));
	}
	
	/**
	 * Add some additional entropy to the internal state of the CSRNG
	 */
	public void addEntropy(final byte[] entropy) {
		if(isNullOrEmpty(entropy)) {
			throw new IllegalArgumentException("Entropy data must not be null or empty!");
		}
		initialize(hmacSha512, generateKey(hmacSha512, entropy));
	}
	
	/* ------------------------------------------------------------------- */
	/* STATIC METHODS                                                      */
	/* ------------------------------------------------------------------- */

	/**
	 * Return the instance of SHA512CSRNG for the calling thread; a thread's instance is created on first call.
	 */
	public static HmacSha512Csrng getInstance() {
		return threadLocal.get();
	}

	/* =================================================================== */
	/* INTERNAL METHODS                                                    */
	/* =================================================================== */

	/**
	 * Generates the specified number of random 64 byte (512 Bit) blocks, using the current key<br>
	 * Performs automatic re-seeding every 2^18 bytes or every ~30 seconds, iff <tt>autoReseed</tt> is enabled.
	 */
	private ByteBuffer generateBlocks(final int blockCount) {
		final long time = autoReseed ? System.currentTimeMillis() : Long.MIN_VALUE;
		final ByteBuffer buffer = ByteBuffer.allocate(Math.multiplyExact(blockCount, SHA512_SIZE));
		try {
			for(int i = 0; i < blockCount; ++i) {
				reseedIfRequired(time);
				buffer.put(hmacSha512.doFinal(longToBytes(counter++)));
			}
			buffer.rewind(); /*required*/
			return buffer;
		} finally {
			addEntropy(counter++); /*change key to avoid later compromises*/
		}
	}

	/**
	 * Re-seed, if either the re-seed timeout has expired or the max. number of blocks was created since
	 */
	private void reseedIfRequired(final long currentTime) {
		if(autoReseed) {
			final long modulus = counter % RESEED_BLOCKS;
			if((currentTime > nextReseedTime) || (modulus == 0)) {
				if(modulus != 0) {
					counter += (RESEED_BLOCKS - modulus);
				}
				nextReseedTime = currentTime + RANDOM_TIMEOUT;
				collectAdditionalEntropy(true);
			}
		}
	}
	
	/**
	 * Collect some additional entropy and add it to the entropy pool of the current instance
	 */
	private void collectAdditionalEntropy(final boolean reseed) {
		final int count = reseed ? 997 : 9973;
		if(!reseed) {
			addEntropy(instanceUniqueId.getAndIncrement());
			addEntropy(System.identityHashCode(this));
			addEntropy(hash64(Thread.currentThread().getId()));
			addEntropy(getDateTimeString());
			addEntropy(timeApproximationOfPi());
			addEntropy(querySystemTimers());
		}
		addEntropy(getProcessTimers());
		addEntropy(getProcessMemoryInfo());
		addEntropy(getProcessCpuTime());
		addEntropy(getThreadCpuUsageInfo());
		addEntropy(getClassLoaderInfo());
		addEntropy(getPhysicalMemoryInfo());
		addEntropy(getGarbageCollectorInfo());
		for(int i = 0; i < count; ++i) {
			Thread.yield();
			addEntropy(querySystemTimers());
		}
	}
	
	/**
	 * Initialize HMAC-SHA512 instance with the given key material
	 */
	private static void initialize(final Mac hmacSha512, final byte[] keyData) {
		final HmacSha512Key key = new HmacSha512Key(keyData);
		try {
			hmacSha512.init(key);
		} catch (final InvalidKeyException e) {
			throw new Error("HMAC-SHA512 instance could not be initialized!", e);
		} finally {
			try {
				key.destroy();
			} catch (final DestroyFailedException e) { }
		}
	}

	/**
	 * Generate key from the given HMAC-SHA512 instance and entropy data
	 */
	private static byte[] generateKey(final Mac hmacSha512, final byte[] entropy) {
		final ByteBuffer temp = ByteBuffer.allocate(2 * SHA512_SIZE);
		for(int i = 0; i < 2; ++i) {
			for(final byte element : entropy) {
				hmacSha512.update(RandomizedBitStrings.RAND_TABLE[i][element & 0xFF]);
			}
			temp.put(hmacSha512.doFinal());
		}
		return temp.array();
	}

	/* ------------------------------------------------------------------- */
	/* ENTROPY SOURCES                                                     */
	/* ------------------------------------------------------------------- */

	/**
	 * Read the current system timers, including the high-resolution time source
	 */
	private static long querySystemTimers() {
		return hash64(System.currentTimeMillis()) + hash64(System.nanoTime());
	}

	/**
	 * Get the current date and time, in a loce-specific format
	 */
	private static byte[] getDateTimeString() {
		final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
		return stringToBytes(dateFormat.format(Date.from(Instant.now())));
	}
	
	/**
	 * Try to get the process (JVM) start time and up-time, in milliseconds
	 */
	private static long getProcessTimers() {
		try {
			final RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
			return hash64(runtimeMXBean.getStartTime()) + hash64(runtimeMXBean.getUptime());
		} catch(final Exception e) {
			return 0xB8C3EF39DE697C81L;
		}
	}

	/**
	 * Try to get information about the current process (JVM instance)
	 */
	private static byte[] getJavaRuntimeInfo() {
		try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			final RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
			outputStream.write(stringToBytes(runtimeMXBean.getName()));
			outputStream.write(stringToBytes(runtimeMXBean.getVmName()));
			outputStream.write(stringToBytes(runtimeMXBean.getVmVendor()));
			outputStream.write(stringToBytes(runtimeMXBean.getVmVersion()));
			outputStream.write(stringToBytes(runtimeMXBean.getSpecName()));
			outputStream.write(stringToBytes(runtimeMXBean.getSpecVendor()));
			outputStream.write(stringToBytes(runtimeMXBean.getSpecVersion()));
			return outputStream.toByteArray();
		} catch(final Exception e) {
			return Base64.getDecoder().decode("AAAAE78qjVTQQGLW4bohSynIdnapY+QAAAAYNIjEImn3V2cxl1ePGXdEMWom0b/V21HKAAAADGPwzFFNjADH9bk2vwAAAArgYSIupRXhlXzdAAAAIkK3NGBdpqecQktuHU0TKO62NCbNxcYpIuxRUh8QQZK2V20AAAASUqX2QlzSYEe9OkTvFmbIKle1AAAAA7/MMg==");
		}
	}

	/**
	 * Try to get information about the underlying operating system
	 */
	private static byte[] getOperatingSystemInfo() {
		try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
			outputStream.write(stringToBytes(operatingSystemMXBean.getArch()));
			outputStream.write(stringToBytes(operatingSystemMXBean.getName()));
			outputStream.write(stringToBytes(operatingSystemMXBean.getVersion()));
			outputStream.write(longToBytes(hash64(Math.round(4294967291L * operatingSystemMXBean.getSystemLoadAverage()))));
			outputStream.write(intToBytes(hash32(operatingSystemMXBean.getAvailableProcessors())));
			return outputStream.toByteArray();
		} catch(final Exception e) {
			return Base64.getDecoder().decode("AAAABSSEzB/oAAAACmHTbOalUC/qp+4AAAAEj1snyZgp04NrmvuoVb4ZWw==");
		}
	}

	/**
	 * Try to get information about the process' memory status
	 */
	private static byte[] getProcessMemoryInfo() {
		try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			final MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
			final MemoryUsage memoryUsage1 = memoryMXBean.getHeapMemoryUsage();
			final MemoryUsage memoryUsage2 = memoryMXBean.getNonHeapMemoryUsage();
			outputStream.write(longToBytes(hash64(memoryUsage1.getInit()) + hash64(memoryUsage2.getInit())));
			outputStream.write(longToBytes(hash64(memoryUsage1.getMax()) + hash64(memoryUsage2.getMax())));
			outputStream.write(longToBytes(hash64(memoryUsage1.getCommitted()) + hash64(memoryUsage2.getCommitted())));
			outputStream.write(longToBytes(hash64(memoryUsage1.getUsed()) + hash64(memoryUsage2.getUsed())));
			outputStream.write(intToBytes(hash32(memoryMXBean.getObjectPendingFinalizationCount())));
			return outputStream.toByteArray();
		} catch (final Exception e) {
			return Base64.getDecoder().decode("nBmp1/PRI/Mfr9KzFhLWLrXkVna+dVNuNItEGo9GBGE96e3R");
		}
	}
	
	/**
	 * Try to get information about the current process' class loader
	 */
	private static long getClassLoaderInfo() {
		try {
			final ClassLoadingMXBean classLdrMXBean = ManagementFactory.getClassLoadingMXBean();
			return hash64(classLdrMXBean.getTotalLoadedClassCount()) + hash64(classLdrMXBean.getUnloadedClassCount()) + hash32(classLdrMXBean.getLoadedClassCount());
		} catch (final Exception e) {
			return 0x4212EB42366712A8L;
		}
	}
	
	/**
	 * Try to get information about the current thread's CPU usage
	 */
	private static long getThreadCpuUsageInfo() {
		try {
			final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
			if(threadMXBean.isCurrentThreadCpuTimeSupported() && threadMXBean.isThreadCpuTimeEnabled()) {
				return hash64(threadMXBean.getCurrentThreadCpuTime()) + hash64(threadMXBean.getCurrentThreadUserTime());
			}
		} catch (final Exception e) { }
		return 0x872E4D3EAE24FC8FL;
	}
	
	/**
	 * Try to get information about the CPU time used by this process
	 */
	private static long getProcessCpuTime() {
		try {
			final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
			if(isInstanceOf(OptionalMethodsHolder.OPERATING_SYSTEM_MX_BEAN, operatingSystemMXBean)) {
				final OptionalLong result = tryInvokeMethodLong(OptionalMethodsHolder.GET_PROCESS_CPU_TIME, operatingSystemMXBean);
				if(result.isPresent()) {
					return hash64(result.getAsLong());
				}
			}
		} catch (final Exception e) { }
		return 0x73B0B40670BF345BL;
	}

	/**
	 * Try to get information about the current process' garbage collector
	 */
	private static byte[] getGarbageCollectorInfo() {
		try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			final List<GarbageCollectorMXBean> gcMXBeanList = ManagementFactory.getGarbageCollectorMXBeans();
			if(!gcMXBeanList.isEmpty()) {
				outputStream.write(intToBytes(gcMXBeanList.size() * 16));
				for(final GarbageCollectorMXBean garbageCollectorMXBean : gcMXBeanList) {
					outputStream.write(longToBytes(hash64(garbageCollectorMXBean.getCollectionCount())));
					outputStream.write(longToBytes(hash64(garbageCollectorMXBean.getCollectionTime())));
				}
				return outputStream.toByteArray();
			}
		} catch (final Exception e) { }
		return Base64.getDecoder().decode("AAAAIB9wUPyXl5zVDasgwVFWhFQ8SjCLGttq/cywRQm9BxJv");
	}
	
	/**
	 * Try to get information about the user currently logged on to the system
	 */
	private static byte[] getCurrentUserInfo() {
		try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			final Object win32System = tryCreateInstance(OptionalMethodsHolder.NT_SYSTEM);
			if(win32System != null) {
				final String userName = tryInvokeMethodString(OptionalMethodsHolder.GET_NT_USER_NAME, win32System);
				final String userId = tryInvokeMethodString(OptionalMethodsHolder.GET_NT_USER_ID, win32System);
				if((userName != null) && (userId != null)) {
					outputStream.write(stringToBytes(userId));
					outputStream.write(stringToBytes(userName));
					return outputStream.toByteArray();
				}
			}
			final Object unixSystem = tryCreateInstance(OptionalMethodsHolder.UNIX_SYSTEM);
			if(unixSystem != null) {
				final String userName = tryInvokeMethodString(OptionalMethodsHolder.GET_UNIX_USER_NAME, unixSystem);
				final OptionalLong userId = tryInvokeMethodLong(OptionalMethodsHolder.GET_UNIX_USER_ID, unixSystem);
				if((userName != null) && userId.isPresent()) {
					outputStream.write(stringToBytes(Long.toHexString(hash64(userId.getAsLong()))));
					outputStream.write(stringToBytes(userName));
					return outputStream.toByteArray();
				}
			}
		} catch (final Exception e) { }
		return Base64.getDecoder().decode("AAAALtClsO0MweNBj0mpSUM+PGVdD6wQGR+VRfgWxTX/murkNQX7rptUXa6qFIfniGMAAAAGysmorpzH");
	}

	/**
	 * Try to get information about the computer's physical memory
	 */
	private static long getPhysicalMemoryInfo() {
		try {
			final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
			if(isInstanceOf(OptionalMethodsHolder.OPERATING_SYSTEM_MX_BEAN, operatingSystemMXBean)) {
				final OptionalLong totalPhysMem = tryInvokeMethodLong(OptionalMethodsHolder.GET_TOTAL_PHYSICAL_MEMORY, operatingSystemMXBean);
				final OptionalLong freePhysMem = tryInvokeMethodLong(OptionalMethodsHolder.GET_FREE_PHYSICAL_MEMORY, operatingSystemMXBean);
				final OptionalLong committedVirtMem = tryInvokeMethodLong(OptionalMethodsHolder.GET_COMMITTED_VIRTUAL_MEMORY, operatingSystemMXBean);
				if(totalPhysMem.isPresent() && freePhysMem.isPresent() && committedVirtMem.isPresent()) {
					return hash64(totalPhysMem.getAsLong()) + hash64(freePhysMem.getAsLong()) + hash64(committedVirtMem.getAsLong());
				}
			}
		} catch (final Exception e) { }
		return 0x29C3D26F9108EC73L;
	}
	
	/**
	 * Try to get information about all local network interfaces
	 */
	private static byte[] getLocalNetworkInfo() {
		try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			final Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while(networkInterfaces.hasMoreElements()) {
				final byte[] interfaceInfo = getNetworkInterfaceInfo(networkInterfaces.nextElement());
				outputStream.write(intToBytes(interfaceInfo.length));
				outputStream.write(interfaceInfo);
			}
			return outputStream.toByteArray();
		} catch(final Exception e) {
			return Base64.getDecoder().decode("AAAAVwAAAAT7uXNkAAAAJaV7QQtpGrCn3Z4oBfN82yb8YlvG5JE0bzrFQ7fze5SdLAAj9WMAAAAGLl+gI/XyAAAABFle8csAAAAQzrA6cZxk0lthAZDPQ4bP7w==");
		}
	}
	
	/**
	 * Try to get information about the specified network interface
	 */
	private static byte[] getNetworkInterfaceInfo(final NetworkInterface networkInterface) {
		try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			outputStream.write(stringToBytes(networkInterface.getName()));
			outputStream.write(stringToBytes(networkInterface.getDisplayName()));
			final byte[] hardwareAddr = networkInterface.getHardwareAddress();
			if(!isNullOrEmpty(hardwareAddr)) {
				outputStream.write(intToBytes(hardwareAddr.length));
				outputStream.write(hardwareAddr);
			}
			final Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
			while(inetAddresses.hasMoreElements()) {
				final byte[] inetAddr = inetAddresses.nextElement().getAddress();
				if(!isNullOrEmpty(inetAddr)) {
					outputStream.write(intToBytes(inetAddr.length));
					outputStream.write(inetAddr);
				}
			}
			return outputStream.toByteArray();
		} catch(final Exception e) {
			return Base64.getDecoder().decode("AAAABPu5c2QAAAAlpXtBC2kasKfdnigF83zbJvxiW8bkkTRvOsVDt/N7lJ0sACP1YwAAAAYuX6Aj9fIAAAAEWV7xywAAABDOsDpxnGTSW2EBkM9Dhs/v");
		}
	}
	
	/**
	 * Get the current process' current working directory
	 */
	private static byte[] getCurrentDirectory() {
		try {
			final Path currentDirectory = Paths.get("").toAbsolutePath();
			return stringToBytes(currentDirectory.toString());
		} catch(final Exception e) { }
		try {
			final String userDirProperty = System.getProperty("user.dir");
			if(userDirProperty != null) {
				return stringToBytes(userDirProperty);
			}
		} catch(final Exception e) { }
		return Base64.getDecoder().decode("AAAAGV6uxD74a5B3+l+mEX4wqs0ynL0fx9tuoLc=");
	}
	
	/**
	 * Measure the time that it takes to approximate value of π
	 */
	private static long timeApproximationOfPi() {
		final double EPSILON = 0.0000025;
		final long startTimestamp = System.nanoTime();
		double pi = 0.0;
		for(long i = 1; i < 9999999L; ++i) {
			if(Math.abs(Math.PI - (4.0 * (pi += Math.pow(-1.0, i + 1.0) / ((2 * i) - 1)))) < EPSILON) {
				break;
			}
		}
		return hash64(System.nanoTime() - startTimestamp);
	}
	
	/**
	 * Load entropy data from file
	 */
	private static byte[] loadEntropyData() {
		final int ENTROPY_DATA_SIZE = 2 * SHA512_SIZE;
		final Path path = getEntropyFilePath();
		if (Files.exists(path)) {
			final Optional<byte[]> data = tryLockFile(path, false, (fileChannel) -> {
				try {
					final ByteBuffer buffer = ByteBuffer.allocate(ENTROPY_DATA_SIZE);
					final int bytesRead = fileChannel.read(buffer);
					if(bytesRead > 0) {
						return (bytesRead < ENTROPY_DATA_SIZE) ? Arrays.copyOfRange(buffer.array(), 0, bytesRead) : buffer.array();
					}
				} catch (final IOException e) { }
				return null;
			});
			if(data.isPresent()) {
				final byte[] entropyData = data.get();
				return (entropyData.length < ENTROPY_DATA_SIZE) ? getInitialEntropyData(entropyData) : entropyData;
			} else {
				LoggerHolder.LOGGER.warning("Failed to load entropy data! [" + path + "]");
			}
		}
		return getInitialEntropyData(null);
	}

	/**
	 * Save entropy data to file
	 */
	private static void saveEntropyData(final byte[] data) {
		final Path path = getEntropyFilePath();
		final Optional<Boolean> success = tryLockFile(path, true, (fileChannel) -> {
			try {
				fileChannel.truncate(data.length);
				fileChannel.write(ByteBuffer.wrap(data));
				return true;
			} catch (final IOException e) {
				return false;
			}
		});
		if(!success.orElse(false)) {
			LoggerHolder.LOGGER.warning("Failed to save entropy data! [" + path + "]");
		}
	}
	
	/**
	 * Determine the path of the persistent entropy pool file
	 */
	private static Path getEntropyFilePath() {
		try {
			final String entropyFile = System.getProperty("hmacsha512csrng.entropy.file");
			if(entropyFile != null) {
				return Paths.get(entropyFile).toAbsolutePath();
			}
		} catch(final Exception e) { }
		try {
			final String userHomeDir = System.getProperty("user.home");
			if(userHomeDir != null) {
				return Paths.get(userHomeDir, ENTROPY_FILE_NAME).toAbsolutePath();
			}
		} catch(final Exception e) { }
		return Paths.get(ENTROPY_FILE_NAME);
	}
	
	/**
	 * Generate the initial entropy data, if it has not been generated yet
	 */
	private static byte[] getInitialEntropyData(final byte[] seed) {
		try {
			final Mac hmacSha512 = Mac.getInstance(HMAC_SHA512);
			initialize(hmacSha512, INITIALIZER.clone());
			initialize(hmacSha512, generateKey(hmacSha512, isNullOrEmpty(seed) ? longToBytes(getProcessTimers()) : seed));
			for(int i = 0; i < 99991; ++i) {
				Thread.yield();
				initialize(hmacSha512, generateKey(hmacSha512, longToBytes(querySystemTimers())));
			}
			return generateKey(hmacSha512, getDateTimeString());
		} catch(final Exception e) {
			throw new Error("Failed to generate the initial entropy data!", e);
		}
	}
	
	/* ------------------------------------------------------------------- */
	/* UTILITY FUNCTIONS                                                   */
	/* ------------------------------------------------------------------- */

	/**
	 * Convert a given long value to byte array; length of array will be 8 bytes
	 */
	private static byte[] longToBytes(final long value) {
		final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		buffer.putLong(value);
		return buffer.array();
	}
	
	/**
	 * Convert a given integer value to byte array; length of array will be 4 bytes
	 */
	private static byte[] intToBytes(final int value) {
		final ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
		buffer.putInt(value);
		return buffer.array();
	}
		
	/**
	 * Convert string to byte array, with leading 'length' field
	 */
	private static byte[] stringToBytes(final String str) {
		if(str != null) {
			final byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
			try(final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
				outputStream.write(intToBytes(bytes.length));
				outputStream.write(bytes);
				return outputStream.toByteArray();
			} catch (final Exception e) { }
		}
		return intToBytes(0);
	}
	
	/**
	 * 32-Bit mixing function; uses a combination of bit shifts and integer multiplication
	 */
	private static int hash32(int key) {
		final int c2 = 0x27D4EB2D;
		key = (key ^ 61) ^ (key >>> 16);
		key = key + (key << 3);
		key = key ^ (key >>> 4);
		key = key * c2;
		return key ^ (key >>> 15);
	}
	
	/**
	 * 64-Bit mixing function; uses a combination of bit shifts and integer multiplication
	 */
	private static long hash64(long key) {
		key = (~key) + (key << 21);
		key = key ^ (key >>> 24);
		key = (key + (key << 3)) + (key << 8);
		key = key ^ (key >>> 14);
		key = (key + (key << 2)) + (key << 4);
		key = key ^ (key >>> 28);
		return key + (key << 31);
	}
	
	/**
	 * Try to lock file for exclusive access, then perform specified operation
	 */
	private static <T> Optional<T> tryLockFile(final Path fileName, final boolean exclusive, final Function<FileChannel, T> fileOperation) {
		for (int i = 0; i < 997; ++i) {
			try {
				if(exclusive) {
					setReadOnlyAttribute(fileName, false);
				}
				try (final RandomAccessFile randomAccessFile = new RandomAccessFile(fileName.toFile(), exclusive ? "rw" : "r")) {
					final FileChannel fileChannel = randomAccessFile.getChannel();
					try (final FileLock lock = fileChannel.tryLock(0L, Long.MAX_VALUE, (!exclusive))) {
						if (lock != null) {
							return Optional.ofNullable(fileOperation.apply(fileChannel));
						}
					}
				}
			} catch (final Exception e) { }
			try {
				Thread.sleep(1);
			} catch (final InterruptedException e) {
				break; /*interrupted*/
			}
		}
		return Optional.empty();
	}
	
	/**
	 * Set or clear read-only flag for the given file
	 */
	private static void setReadOnlyAttribute(final Path path, final boolean readOnly) {
		try {
			path.toFile().setWritable(!readOnly);
		} catch(final Exception e) { }
	}

	/**
	 * Try to resolve the specified class, by name
	 */
	private static Class<?> tryResolveClassByName(final String className) {
		try {
			return Class.forName(className);
		} catch (final Throwable e) {
			return null;
		}
	}

	/**
	 * Try to resolve the specified method from the specified class, by name
	 */
	private static Method tryResolveMethodByName(final Class<?> clazz, final String methodName) {
		try {
			if(clazz != null) {
				return clazz.getMethod(methodName);
			}
		} catch (final Exception e) { }
		return null;
	}

	/*
	 * Try to create a new instance of the specified class
	 */
	private static Object tryCreateInstance(final Class<?> clazz) {
		try {
			if(clazz != null) {
				return clazz.getDeclaredConstructor().newInstance();
			}
		} catch(final Throwable e) { }
		return null;
	}
	
	/**
	 * Check whether the given object is an instance of the given class
	 */
	private static boolean isInstanceOf(final Class<?> clazz, final Object instance) {
		return ((clazz != null) && (instance != null)) ? clazz.isInstance(instance) : false;
	}

	/**
	 * Try to invoke the specified method on the specified object (instance)
	 */
	private static OptionalLong tryInvokeMethodLong(final Method method, final Object instance) {
		if((method != null) && (instance != null)) {
			try {
				return OptionalLong.of((long)method.invoke(instance));
			} catch (final Exception  e) { }
		}
		return OptionalLong.empty();
	}
	
	/**
	 * Try to invoke the specified method on the specified object (instance)
	 */
	private static String tryInvokeMethodString(final Method method, final Object instance) {
		if((method != null) && (instance != null)) {
			try {
				return (String) method.invoke(instance);
			} catch (final Exception  e) { }
		}
		return null;
	}

	/**
	 * Integer division with rounding up<br>
	 * Note that this method does <b>not</b> support negative values!
	 */
	private static int divideRoundUp(final int dividend, final int divisor) {
		assert (dividend >= 0) && (divisor > 0);
		return (dividend == 0) ? 0 : ((dividend - 1) / divisor + 1);
	}
	
	/**
	 * Check if given byte array is NULL or empty
	 */
	private static boolean isNullOrEmpty(final byte[] arr) {
		return (arr == null) || (arr.length < 1);
	}
	
	/* ------------------------------------------------------------------- */
	/* CONSTANT DATA                                                       */
	/* ------------------------------------------------------------------- */
	
	/**
	 * Two tables of 256 pre-computed bit-strings, each with a length of 512 bit (64 byte)<br>
	 * Each bit-strings has a Hamming distance of at least 240 bits to any other bit-string in the table.<br>
	 * The program used to generate these tables can be found in <tt>etc/GenerateTables</tt>.
	 */
	private static class RandomizedBitStrings {
		static final byte[][][] RAND_TABLE;
		static {
			final Decoder decoder = Base64.getDecoder();
			RAND_TABLE = new byte[][][] {
				{
					decoder.decode("EkvjrThmlQfa4gZBedtSnzx2IWVBiyU9hR2kdVoJAxQnAYoDBiezyppRDHyLOlXsxheE5H0ZQ3DPYbmhQdvHoQ=="), /*00*/
					decoder.decode("1TXsgJW3fwXtJn7C0S1kJTPJGu2uAnJ3FPPgSPqWOJxDyVzjIbQO+PuWAuPOVw0Wt03+uwZ/+oxPOBdv9FDBmQ=="), /*01*/
					decoder.decode("ee1gU1hV6vRo3oDvqmv/HB/AYwpsjjW0yjA1ZW0kOOt70c6m+npMuJAJt1CUQrcrJMK+z/4rA33Kod94wrG0iw=="), /*02*/
					decoder.decode("fS6TRRaQbj76C00VUg1mvYck+bqh/yaoG0z1PgGIWAEa5AyR9+kRYljth6VZ2KkIPYQn7LfAhtoxjmvnpuKgQg=="), /*03*/
					decoder.decode("h3aSMUiV8MAa4p8ecEjxCb7Ia95QOmoueJyM+KFAK1Isucd0IkxaHlbdGmnmReVBjTwTb0himeh7SWVSitj28g=="), /*04*/
					decoder.decode("t5wCqrQx83s1pBSb6odjrsSJ00z27WdpGusRWNE+mcOCrbb7+aopTxWLRR9yXSitrg5S6Nxh9yySYh/khN0sqA=="), /*05*/
					decoder.decode("oBTFORDT1f7QPjFkfUC+jPix+cgePYIOjrx958+xBCp/HugViaaVf25dgy+ktbqeldpU9Pyhkb5ngYbJEVcUew=="), /*06*/
					decoder.decode("OLlgbI/lhhRAvhofWY2+2qlUbTbZeLdMlhrfN7oDx4GZiRH2bB7Wh9oepesc8xzNVqh0GZlrnOG/9JZ6/SKR2w=="), /*07*/
					decoder.decode("H59U7+aPJWA9OZAjYWkNcl4b0I4JMOy41C7iaZAKYe/9VFlUMySMme5LH8hn1N7Y9Bzj6P5YHtvHHdUtiweAEQ=="), /*08*/
					decoder.decode("y7VxAWFcT4iKYx4zD0XyPH5/mRK0Uj95Pb8hyi9w0tWDPrtPfRm8Qp3nWhNY+bV+kct86cDdqJMFT9iiEaS9WQ=="), /*09*/
					decoder.decode("j0ZcOe0VXKqECPT2arwtTQTJG7NGb1cL7M4Gnvs2MqrsJs5thcDM03V4XBT7ZrhAca5ianfMMlK7bo8lU9GAug=="), /*0A*/
					decoder.decode("92SkiUa10TTS3zCw+1m5ExTzHj2VXU5qJgdM3wZVWmH0l5eCXaOSFKsijgfhVj5yIsUTXBW5AU8xf9FRmivP3w=="), /*0B*/
					decoder.decode("XNZLsYBOO4m71piEJBD35KuqsWw9OF6aqFkz7Dms4+uAA9fwXERCiSp61jG1WuzYGfRNmEfL/3fmwwkzH0PfWQ=="), /*0C*/
					decoder.decode("YETZZrWHEGsQhPRD6xNr1LxFdWRxkwHXbBOxdPAC3XKZEV+wFUStKqeFiIKllzY9yD9zrIP0DbfMjjpVIjjiHw=="), /*0D*/
					decoder.decode("R7IIpRCrz3BUubfQH+ZKdgqMpj5no9ZGfE4/h6/wHUaXEPDvJS31wneqGx1h4KtjcgbQYvGvSWODonM+fDr87A=="), /*0E*/
					decoder.decode("7oLkdx+uKZft4Tte7b479+vhQp3iaqtyc2p6PeHchvD2NtjZIlnqohFE/KdVGCwdLW+2rhGJl5QadBveX14Erw=="), /*0F*/
					decoder.decode("27gmCGpkwMLhe27Ww+psWNX5GM40P9VL2xiBPHYToFbcgRT4CuAXHDAuGoBDgnziaTe56N+A5HEU9wVMrZLzoA=="), /*10*/
					decoder.decode("0IkCB8Oz/oxLIS6HQOqPVr+Kbpyc4EvZ2HO6fDfP/2KJNUIH34paqK28VUbvE/AFDqFyEKB5wwcVGmOYeZ5dPA=="), /*11*/
					decoder.decode("XtsnjZniWZO50l68W+uw+NTxFzkBk4qCYgaM5Ljfd+8sGLt4e/+geJ+PxYF80qo3qzI0+f03PTsV8bjcp90oDA=="), /*12*/
					decoder.decode("7VRIGgPZuaheeKe4914AVRu0Le2W9jsAASAxO3L92YF0tuX+6jmNeB/vbgwQC8revdxMR4tkqjbPqOPlTlAg4A=="), /*13*/
					decoder.decode("HAyiwj/HZGZncvfF/oJU2bNrviMm/vkCepl/12Ad9gsGPa788buiKBjS80OtNY0dbaBXp0hen45YHIKhscFMJA=="), /*14*/
					decoder.decode("LClA/u6vlOYwL/EhfEdmj7ulYgnv8u3eb9S7s51evAlx3m6dlNycQhLRczx7MCbH/o2gIv5G5mwJ5p1mPw8aRg=="), /*15*/
					decoder.decode("4ltkH02/cp4qggYOjTYJ3eGPfgh3/ltEUoVrzh3iM48PHfA30BTMRm5wCrGNHR/+suyQ21j0j6PZgzRXv77NZA=="), /*16*/
					decoder.decode("oOM6zduszD558SjlbZOk2VWPn3bScWTDNAGm6u1sBLT6bWqA+tEqMkOeC7GGQX2OY8gJi8yQoTn3+59B8QBZXA=="), /*17*/
					decoder.decode("XuLOLToDJ4t3us2Mh37QHhnF0huPBNycPz2OEEexx8kI4oGyNf1R7rCTuy7tnx671/mijoF+TSPZPL1fc+iqCw=="), /*18*/
					decoder.decode("lb7n5cV0o2+SW0IeDcDKTYRB2CXwGLVYkwg7jbsi12eNYoKFAiRp7ENUqp8BZDs28e/GuEOyMT4STe+Z+o727g=="), /*19*/
					decoder.decode("j3FuTH22SFRKADtyKp9Y3xgA+7CmQWGt7fyTQnX1DpmWptxhaP7ACLkGd2imeBLjX1HtgSnZ78sgnbAp8RSS9g=="), /*1A*/
					decoder.decode("RfAlMJD1fQOyQ6cNIHLJQk3m+V43GFaDx3LTx9B1xtpJmWEJeW2eMsUf3p3ozwiInjjsDtVDsaR4e4bGoT0jpA=="), /*1B*/
					decoder.decode("Pd8+HvPZMc4jBEfpna8jMTabSRiRWewcgpoEgoUtpZxqnmusvKJ0iJdp9XXXcV0sYGhj0Am2PnBxHvYDzSwZMw=="), /*1C*/
					decoder.decode("GgISpO72MCH9begnKk9/adkt/xWC0xH70nLQmO0wrxWiSK0qdFJ0lcpbB3/Cs6wGFRjvxnU1eOd1nSDxSh6eFA=="), /*1D*/
					decoder.decode("YqaZX3VtJoPt3MsrqxTozcuA6S5wAVR+6mKa5XcUOZtnhSoaWwaTk7+s9m/P9WZAIWgV7uBY8g6KuH6yPb4DvQ=="), /*1E*/
					decoder.decode("f/rYcY3ZawW1TiJLt/xgrrjJEV5ke4lcPunDN3mL75ANg3gbvqNnXTHYUQ+XgbXJ3Z9tJS2ykLSrBXwtsCpFwg=="), /*1F*/
					decoder.decode("1oCgklySEjpyzgb3ct7AjXi1RxvfP4hzl2ATj02goLd0YeI+D0I+TmOmGexEdlArW1TnsCFmeaHvS7higem9fw=="), /*20*/
					decoder.decode("uBJJ51BBYouz4L/Bvg2T5Bwhh2PX9+9Vbk+oZF1zS1R4kcKkCIkAV87bHVazTuMYhZJuc6guUA0songMaFEVnA=="), /*21*/
					decoder.decode("UQptQoGM/F9DBNlCo94yGSxeZWHnj5fM+L1yyzug7Qtl5hfFU9bfNiioTfHMtMQAUxqI7FbnWaCHJglPBfS00Q=="), /*22*/
					decoder.decode("SssDO5cmIMyLDNE57w837+RAHqu4GjpnX+HXxqoo+wDfyhLyZu04r9QxwL5TiHI0eWk7SP1P7dOw4lXZYpHWnw=="), /*23*/
					decoder.decode("m176/WeEgv0DbSQ4HuWdvtq4wrNQx5hQ4U+GIhZMwrqTOM/bLrT5obNt946V1svVK7jZM8G4a5WXKNt1vcoq4w=="), /*24*/
					decoder.decode("p6Ro1e9HYf1j+J2hZuZdyABqVxQ/jnLkv3hYKTqiiPxF37BSAnPnfk2XFnmzS8VkLnWyBBJWSCr/hYrXMpyAQw=="), /*25*/
					decoder.decode("vviT+uPuMoCweII++TaWmO78RJCrrGTE/2kG2NNJGOSh7QyVgxQSN9/FlC1IAVMz5fIfz6boBOutFZpL86vzrw=="), /*26*/
					decoder.decode("yKCU+PBE+sjCQ11SDMToV0SKTWtkk2+5XYpNqtasToTnSs4QtD9fc+m06yHYp0sxI+ltVx0nAo9hiRBZD2Be4A=="), /*27*/
					decoder.decode("rY7aoTxj7/koFMEJBUuZl1SEjsnHRA1WY63Z4yhbJsqqIxd2Z23M0QWV0tMM9J3oodZakHdNadFiAchOpZABlA=="), /*28*/
					decoder.decode("SCFeZM54iqF3afDSOjfHo+Tg5AU6Rd3Rk2KvgkYmUbLq3TFhY3AC9jOVF6vAjXfk/k8MqjBedloZqD39EG11cg=="), /*29*/
					decoder.decode("VTOc4Jm42IjvDlIHGbm+FjnU/o19ZP2E3rBhhYab8NFAYogbC6rnAX/Z/Ts+SuoioSeO3TvSJLskUuZEKMux8A=="), /*2A*/
					decoder.decode("sY2ErlGrB1SpsfZNUUwG3Ta68Vxp4b/hrUH1FYK3EOfeMXPWvB2I+kLcylNaDLIS/sPZdUYs1wQSThjJy7MXog=="), /*2B*/
					decoder.decode("XiT1gR1Bw9AnWK793P9Z0CCmaYNRk7sUpMXSW0mLhFfDlxXRm0od9vfKG3zfP4UPoDx977fg9PRMtXPSlUmCng=="), /*2C*/
					decoder.decode("WAy0AoHcPEmei8y5a8bQc8Ej5h5S+6qMoj3/W8Xa0gV8HRmcL8eIpjTXzqHm7PHKyVqIvGjJK0vqkv0x/S5nTA=="), /*2D*/
					decoder.decode("qfeAGApLFB3H2aESFd6es7W+ZakBbyR8ifL00ioReqBuKEZ7p5tkIfsmG1kV4SjC82jm2kWtlMqOKTjEtzlmIw=="), /*2E*/
					decoder.decode("/OY9Ptv9wsnLvYQObCjmO/5ek+AlEIGkYNcCZUif0CWfogAgEqjGtkLcHOIhkdeMubi8snZU6+8rQrKRf6ZnGQ=="), /*2F*/
					decoder.decode("MqV50LJEZR2TH2Lxru0D9PcXL+BPOB6N9V+cBrOPx4t/JJmdSJtg76tgK5+INBoRyHGBmKrPgkonhLSAQ/xLdQ=="), /*30*/
					decoder.decode("oqCNQ4aAkHEr93PX41m0NcysJdwQ54Uxktw2SpJEzve+wmJGNQrSOBVWMLbd04uD8MXvVZCnvwe5QmW3REQw0g=="), /*31*/
					decoder.decode("aQFB8s47xVkFTbdvJvonjlwUnNk1uV7GmpOO7KYkOZUIBQ9eMvEBEaMQNUBTOEgh6ixnMwDVQK9YlA5zl6NaDA=="), /*32*/
					decoder.decode("htBc4gLX5ZXNpe0yeDYxapaKOXKWaEWmCxDN3PiCis2wcQC9Xzkzu5XkClpK40F+f5oqWdNVXRX1551uCkEfTQ=="), /*33*/
					decoder.decode("8D+bkYCA+yGM85FdDs/uwk3ySiOggYMZt6coE1CNZGoOr2j3sCXzW2AClaKrH43ruIBT3VOoQ/4Jz9Ymys7TNA=="), /*34*/
					decoder.decode("lMxCwGQXvFFxb450UCaSLr42Oyho3Y+tZnguA5oFmDzJRgcrD80F/KSo/+j34S9LH1BY3Oy///1fvygu/LXFPQ=="), /*35*/
					decoder.decode("Gct0SmjAi6c8C3VfJeL3bjNm2I4z9ylJdMXjgeqD+3iUSeH5sl5oIs2DZVL7RKnbQ1QXSqBAShZq8VvHfoSGiA=="), /*36*/
					decoder.decode("BY2LfbCYW8/MLegYjS+23usK7FMyXqWbenPdiJGlItcRuZ4J+e8I9dfZxWkD6dxB5EsvCyr4g8xYU+EIk3QoPQ=="), /*37*/
					decoder.decode("RT9Ncm8kz0Cit4nYcwoVyvXAsq7K1x3hS/dG6xW1AUQEovkTTDso64aEEHY0b8qIJE4n+6ZtDCHr9AFLQXdYcA=="), /*38*/
					decoder.decode("I8wh6nJUS8RqrqAmXi4BVYUfF//+YODnhBtuEsY0Lelbeu5RHkDtijJ+8A4sIARrD+0JcJLbLiXvo4OnBe7Dcw=="), /*39*/
					decoder.decode("sj4U3KJEX0RR3IZmNFRzEsUkkHvNoW5nCJpxTxzjA/+XIhCxlxV/wsyIU+FGl8TOGcFXyWSigmu/Fk6h0JXqBA=="), /*3A*/
					decoder.decode("IwgiAYJ1CQYezw8sze72tk76BNFHOwNRCsaEUajvBror2HcvCYYd0/G8JpND38UjylTKw2ZpIk2F5m3LKSLdVg=="), /*3B*/
					decoder.decode("UOs2uPV7xmtgypWHxWO5gBtlvGPvNeva4FbYmEtV5vce0XKKw7h0XXaybeqsOJjwqRtlnm6qjgXk+fCVCSP/BQ=="), /*3C*/
					decoder.decode("rFm0Ddu+H5ndAMUKJmz+rSEqh9ZqejFeiNgdGSBZG26e9B+K35Q/fF1G+RRQu+bz1YBD9FqUsUk5nO9eVmQFLQ=="), /*3D*/
					decoder.decode("LPWKDh1Be0tZFA55xzOrjcLbnvN2dcpoZJrNOBk/ZVqs6JCFvu0jCqxUGnRgXgY1axXrFKBhAKHlOUn0F/fPRQ=="), /*3E*/
					decoder.decode("wiDrWEbaPz7pehT2i5h45ppTheC6zEY8QEc+t34Tld2Dd0u5+PYMFydj8aQR1ZsOPgRss4HlFVQaU3kf2wLK3A=="), /*3F*/
					decoder.decode("XWRiH0gqKLQdYp2T7sFEChtUJPiOZq9hrhvFh2vg8AQNRwRx66wQ525r1GWPS9FzxKfRMrfDPmnmMoOeCG4ZQg=="), /*40*/
					decoder.decode("HUeBp2a9DOT2uUHbGxfTu2oAa9rRK7brusIUVvlMcfmBJ+NT4d8MXiYrMMksPtfMSxZwMy70LodW4OesPobyrQ=="), /*41*/
					decoder.decode("dT/rL1O72REF9UXN6sPAXvrVDx+Lrh+LEJITwfNIPuYYQ7H4zak6Mq+1Z3Gann/sft+t1aQKeOzsBzyDzLDGfA=="), /*42*/
					decoder.decode("wlavjj9ARICNouDapmUdec/pn0jTWo47lOTU8SVIZnV3XkbkRSo38LJIaTb8ZGrsPG0FfIMUiWriBBo4OHygLg=="), /*43*/
					decoder.decode("ESIXy3Nu2XQy6furSjBTRu+ySY4/usCSQn1oYf2vrszvJuQjq6dmIHBj39q2AGplJv6/ki8EhCZ+Plgj/Ok2Fg=="), /*44*/
					decoder.decode("MvBE3xd5Sh56N0oRgsyPbFAFstCYXAcvRKnoOQYHCJezYVnGN0xnwQRs2IfjovnsU+X0zDhRMHS2fyaeAikcZA=="), /*45*/
					decoder.decode("OMvMyQedKySPkKua/dAxWqyUdKthGPuHociocLfK2k4CHq0awOHmllG5iEcgRRHia4PfleZf1LA+YwsInOTxPQ=="), /*46*/
					decoder.decode("vh4wwc4CF79OabKq2i/I/mEyuVDs8dBaZ6wpJb7A2XbkPKz5yUKGiMIjiN0OmAOJvzwrcfDQzQXIgV6cblOCdA=="), /*47*/
					decoder.decode("Br1zIzReaUYhrFggoEHxXjS+/MbVXjLxqua3hk9DoanklozVhWroOwlsPYts744uDhWFJwZ6NZh/lpJQGwVhrw=="), /*48*/
					decoder.decode("Kd+L/IOS43SSzlAmw+3XEGkmVY8/rEr6jeCOd6faEPoVoI3v+h8IPDLFMpRzvu4OwR18dAEPtRe+xrporBhRGA=="), /*49*/
					decoder.decode("tuvfkP4l02xEqOVya+pbWNnBNxXP34Un0uHgDkLIRDs4jDux6s9wnp/0tAY07731aqQaKUAm7dQbBEXDjPH7Tw=="), /*4A*/
					decoder.decode("FdUfmzNAgrf1/RvRkLHpjNFV6rmt8KvOCNkKO5iIZnrVBjLHO1HsLRG9qHg4yG9ofZOPDzttXlk8mPt5kRZ0iQ=="), /*4B*/
					decoder.decode("gf42OT7ZTtKcNnbNIEvpS8yFFbOS20V5Plv3VjXTty4Y7g9u2FFbRaSjuAuomsKw6H6MPnIzWsgM+nvaXtC0jg=="), /*4C*/
					decoder.decode("llVVEs9OrQYRmxBnKca1rONLROFAk15TvWRJTQKbdkLKEuDqw4kopFr6CMCPrfqpz2gmrx/b5+xSqGZuDmfp0g=="), /*4D*/
					decoder.decode("9CdqXwsK3Ap0EQYqpPiTAWNGuA/z1ZWaBQWkCATerYoQhg9vdrKSIipOn14SLDyED6+S1+iL3GBL87AphIEpyw=="), /*4E*/
					decoder.decode("kKtsJ62GPjJv8A6hnoXe5jnMR/Nvl8QnqIbjLKfHKAlGq54NWcTLbXsaaCMZy6vxiHUlVYSRG07dp6iOoDLrug=="), /*4F*/
					decoder.decode("Sz8/VcP5wed72Lmqy5rWBMRypepuoThl3NJ/Nt/LmyI6Up/Yztjnul+KuwvoYmoI5IMQtrQM84VQ80DHiRAHVQ=="), /*50*/
					decoder.decode("S/rfP3zG97bai2MH7KSwbOL3fA0bJGlpnGZgexqF4Q9iFHAptdcTl4EKYxsbS9uUoZVehycOO6sVSP1wA1Iruw=="), /*51*/
					decoder.decode("wgIYcGt7RBKE1JaH0t4wifEvCuabgGo/OOFemE2tXT4dbJk9+Z3iOddQQsSS0YRMsr7NyNhat3UpIdXWYe4jAg=="), /*52*/
					decoder.decode("lpn1/C95tA2MfzjVCmj9mSG2YW/Rboq9/pBTmPkVnqety9bSakUuW4th7JoXEnAmrdxZJyTfC7/t7mtLYBW4tA=="), /*53*/
					decoder.decode("YZoqtydmdoiDqQCdIp3gbwuusJfZK+YnLPiKriFW0wQb8JZFg2u+8Pf3dEUnhN4PrImKonEXLkSQONyHoXe7Vw=="), /*54*/
					decoder.decode("OcgNhmyrnrzfBybvvYcITIMGuNFkCrkQBldLtYzLvBD+WwtqPGHo2CaHh0hMKJH2Mig5mAUvMTEz1S3V8gdyeQ=="), /*55*/
					decoder.decode("oWaHEpw4z7kQM90yvZHh7yY9IhmDFv2CeLpkQFha7lOSF2FpQLUX/U8rna3OqifHkXdJm4WVzSCzSmRj1lLeEw=="), /*56*/
					decoder.decode("UNz8DVgSY412ZGjBgJyagA9tVCYy6aThiqASNzucSZWsDhHIibJ8xw70qfk5EumHOnHDAwLKk4ziaybG+8RLoA=="), /*57*/
					decoder.decode("W9ZrrdGLmIxBJYJCag2lS4J5lIh3r0nJu0b0oq29Xo32/oPnIlcUprIgDEdye3F+SJbU3Wh0P71ueU3L/z1meg=="), /*58*/
					decoder.decode("j1rLUZraT6LnlVFKVFyDffxmPCqM0XgMz5yyDgUx1sKQFduniynYzLOcxOZXd4ruinDSJ3muIWye7IMOu1kW9Q=="), /*59*/
					decoder.decode("Ro2Sj6EJG/fqrvnRB7+ooxFSrdHLREqzFW9wzA42y6NS1Z2kaImyk199cJUfJzchu+LC0Q/c78OLlf791ac/aA=="), /*5A*/
					decoder.decode("/gfsOzDXm93R8tq4RXqduOltp3PtFuf1DQgSLOPHy0ur3A9lO1keg3fzSajAUmYeIGLO5XB/DQHijKPCgAlh1Q=="), /*5B*/
					decoder.decode("r0B9upTcBVP0WJD8NuoVt+6kT2cu4RtU7tDhfMV4vFq+qpzH0FaHKWCGy1vbzFQoV4HfgXN2f+h6Zg52alJgMg=="), /*5C*/
					decoder.decode("kvcG9iEQPitKK+vACvIi5QapTgra2COXLbcJ6YtzVjKhfl58roO6nHEOxCdfh8ThdpWTLwqGxfdm+Hdx42nOjw=="), /*5D*/
					decoder.decode("sPasMe4vp+jHfiwYIr/ksclcgxk8HGwufJ9av9hCZr0d1O1hbuYiY9GR+QhfMsmmdgzA6ce4hC3TyRHIWbN3QA=="), /*5E*/
					decoder.decode("+M/h4mH8BqjFeipRUUL2Jtx7p8e7i2zim8sYfk1NL7xUHlJsPyOIMNtcoviJTHPfLqDDwXwFrO2Cfig4zA6NaA=="), /*5F*/
					decoder.decode("u2+AXzVE9VuUPy/EZT9HVvC5ZbMtAuAz2zdkit0jEkXqw0FsSb6tUR3Y8ercBtoM5P3ShCrZJOeR4zb+KvXXhw=="), /*60*/
					decoder.decode("iXlmkzO2A+y1BqSJ8c8Ad7HwqVYiZ4k8Tal7tuv/rYBdb+FOw5QRzfB49rD3fEhzD5Ma/x8mWRF2yRsz5tdv3g=="), /*61*/
					decoder.decode("IvVksKolt+itxXfF3c0riCs700V8quZzdAEnPyLQns8lqRSivqruJvxTYAZ/KepSH+6y3rmeOeodGyg3CIa+rQ=="), /*62*/
					decoder.decode("tN0hbJDhL5bEFIBbXkGSKYCszE6c4UWrxQ0z0J3YQlrKScwCHhcnN7yrKX/Htsghvx82EK+cIC8UfLHH/IGzwg=="), /*63*/
					decoder.decode("hCIqK27TjCM7Sp4HUnWlbu56c9bj/ScOpjXNnpceBU7ubbrZbCccYgutBQ+dwEs94V22lpua1JEGWNBcd7EdOw=="), /*64*/
					decoder.decode("g9uqEf7VdnDsHluJ7Fi24dzuPtL/JDbZwyJBia8aPTW0a2K04R0FYlj3usXIcwtAGKEuLAzuULbYMXOVMd646Q=="), /*65*/
					decoder.decode("5lnclUKQLjAWrwENvOIQaR9QrD9oz5BOJS4v07VwA6ZvSTxSFWvtByQahHjclJac2GEyGBaUnYh+T0u62Xg/Xw=="), /*66*/
					decoder.decode("D3ib9lgfXbiv4u1l6DPP9yaJCp3pdNuhMe7uKP60eOwduUMwsFe3QKfk4u8FKYHh0Imp/aapSFoUGltOcpu+yA=="), /*67*/
					decoder.decode("ZFaUD8pQVlPdx/ghZ0d84JuBQk95ecgMX7uko9RxuQ1CZgOiCLUuv8yRxVcZqR1H+1b0UwNP91Z1h8OJoHNr8g=="), /*68*/
					decoder.decode("bAOs/K5NQeMt/aflQCBwuVIfLg5xXaWuTanRGrX9Ol/69zFAT0IzsNQRLgWBLEaqH8uLPGOFAZodt+KyP+g80Q=="), /*69*/
					decoder.decode("ABc82hG4d4MibDq1pQoF/pjdcHoBRcx2eRb1eGPuRnilBn2LBXcIlKYSzc2Q9K5v4twwe6cAkm83hKti5KRj6Q=="), /*6A*/
					decoder.decode("dWlMSKIp5140sTT7CY9KNsTJCQkR+hDEaBATrpAbQqMCuEhSTloCIi2svBKnCRsGV8PFV73kwwXHTcymFO+Dzw=="), /*6B*/
					decoder.decode("ZNssy9h3bFPu+2HDHKPiNOoZzSOMFpyxE6wlkpdNqdm7vwWzZiTSsgo4N0O8tOtVGKfQft+T3Q8/2LfeTPIHAg=="), /*6C*/
					decoder.decode("GNenkezPh2WgkkxiYtQzoILersmhhEHdQmyTuTFsxKaLkT7pcD17mQgUX75Cbhe8XCCPIqAV0ycKpGUOaB1MOg=="), /*6D*/
					decoder.decode("gXzM7DOlqijKDPFvJm4jlYJiNu9QUtqfMWQ3XsZJsz9w4vBFDgSDxeEvX9DFtbAyZfL6T+gTITziMFu+2SiVMA=="), /*6E*/
					decoder.decode("lvTZRUkKQSjZBGKxaTg8pXA8u1SFuYF3mSJvVY1vNhPR00+j5FmViz/nF/Yifd4jpRJekEZoXr0bq1Es2irHrw=="), /*6F*/
					decoder.decode("uOm9QoaQnM9I+fJ32JyMUDZc0nWdX+5dmfaN/5sG2Gyy57xj1i6m6hi5FDrzMUguQVoaLAlVJ8gmhzzA3uc4gg=="), /*70*/
					decoder.decode("259vfENS3u+YTOR0XTh5q3njnqCOCIfw3l0pkKVaoGpYQbK+FyjKb5lMUvZ2WE2L3c+QTZG06qwpEYznoz1dmA=="), /*71*/
					decoder.decode("dk5TEvATuuad5IhpzDU0FAQbkpmbj0q921ID6sU+jzSXMEU+P+XDHeJ0czt+nCbFUQd8AnuARF4eC+fzeAxx5w=="), /*72*/
					decoder.decode("aY2HNofgzbrz8x+XOjZfMIxN/OJO6QvkAw1vXeh8ifnU71/3JJZweqbmvwUOdmOtlDzdxhvdGCgNggq+/oy7QA=="), /*73*/
					decoder.decode("1jATIGMKn/MSlPEeZUiPLQ95RBDaxJWeCkvBIs86JaTTr1L8WspxKYPCLrAaU2sIk9t3K/d1pBPDDPuRJVGcpQ=="), /*74*/
					decoder.decode("rVlL+PhDq781X0H1Mb3asN+l8z2Du7MUNYE9fI8jVHD9V9QA9ecq3M2hagOTI5vFw6Bmn0hJDJaZlxz5i8qYdw=="), /*75*/
					decoder.decode("SFNgdb95scrQthOamd5d6qeDfAS878zL0o/NHVBOscQfisLrzGQV+MeceSgp95Dc9hVjB8YPdzoV+e0WBhhUIQ=="), /*76*/
					decoder.decode("N/yGG0utaCUOk2akOHJefPfQdbeTkYg8minVq3pXmcr2gDINdAKtOFGfyZYV8Ge0ZoufuAbREoJo1BbyEgJKEQ=="), /*77*/
					decoder.decode("gjQYxn8Fj4Tt/x8WvjtXvx8l1OmDfhIwv6TR6MK/y9vCsbt7ldJSp/zh68Tq+QKReBlTUiFfsvQO08tQIHu0tA=="), /*78*/
					decoder.decode("ex+/JkktdBiXNchY+BiMssPLecO/41JleNzcg8zIFdJAQC7vppd2myN9n8XEg6PL/uSx2U8+ZzdV90IoG9qA3w=="), /*79*/
					decoder.decode("qsUUVsx8xX6OrFHnudCP+csda+UdtxJGNTNSYp8NC90shc45V8hmRRivx1kwwGS6yougTIyIUjeoLFEDAEWVjg=="), /*7A*/
					decoder.decode("WfuTw76PlYxw12cO3iHI14AboPScKeKxbEVLI40PNB7S40nZhKN+D+y8oNsB9Fc133a8T9T5nWzGaNQLt0C5GQ=="), /*7B*/
					decoder.decode("ir/X++peAqqKKU6w8GxOu5GNBCqFwfoKtu86ZeQumqUirxuSCtkr6yd94hLt4B1OAu8PDxcoHiwTMuxAi7e/cg=="), /*7C*/
					decoder.decode("UHQftN2rB8ttUErSEZtmnWrOHDTFmj+iAxyuvsz7his/wTSYdQBVSURCBA22olOB+VlfKZnMP0aRqJ6LH6ZSlw=="), /*7D*/
					decoder.decode("RFVeVU6kupVKIqfqRCtrJVrlRBNOEVgX0LlUCr5gZifE+en3g3CdJqwG6uiv8G4O67U8pp+YSnCK3XWLeo8uKw=="), /*7E*/
					decoder.decode("CCPKJscCFdYGUw38KpCoPD9DgBGYYYBVGoguJYqVPHXYDN5QdcWxKkjZkE+07f6ULBVrw09L8xJAI9tVn/21vA=="), /*7F*/
					decoder.decode("1IlK/Gld4T51krJ0R+g1bw8YJhYNg1ACH/Tr68gPjXkgmo+MNCTf9IEFGpOGzLKVEBzPezt2eFJpa0nJtH1XDg=="), /*80*/
					decoder.decode("YmHyDNS0Bsy89UfIK3U0ULzho6cr2rmMATObzUcQJDjt3mvfI9eDRp3PG4orB6iintt/sA6ri+JDesSuh8G4tA=="), /*81*/
					decoder.decode("ljb1UA2pWo6U+UXvW4aKy74LQfLbajVfwM9o8WNZkqNUmeajcWKm2YIlVmWWQI+VGsKIhjSz1bnDJuckxcUBSQ=="), /*82*/
					decoder.decode("kI7AvgSFglIUG5DKmSvu5TDb6jy3SsmpoZ3A+D1eixJy9ZWt3hl7Vo0U8GbYAKYOZBAcn1roypbsjJAonOsQGw=="), /*83*/
					decoder.decode("v8MzDrETgQk+Hls514otIjM9jGO8Wr64WGxzZguyZj4oAmIpm2ryzEkmvFI6giSQGpyc5VeXGsnV6zuHjiWhMA=="), /*84*/
					decoder.decode("AK6cX2Fo9hO/bGvX09lkL+bBFSQJxGGGbbkz6sx6bk1GEJeZumr97X7bH+AUrxtQVkFaEMUSo7jfdAHpRT+oSw=="), /*85*/
					decoder.decode("QRzOi4lIFWbeR8JeISVTeu1JyDFuDS6yCLGj6LBiFzplwwBK2FeRbmBtd/8TAtb3hgDnuPzxfoetucJebUtoTw=="), /*86*/
					decoder.decode("FyF2s67+GuqoHW46F6CIIumRM7qJsfCjF9AW2Er6VZt/3vb5QxG3WiBo7yKlu+2o4YW4QJ2C2uqvV5mXEU1yzQ=="), /*87*/
					decoder.decode("bqY907OnW3JkH80EM/JNdxGCwWAibqNqgXYWPJG403+QfR9oF3ooN2LsIHscmhtmQi7viOsP8AOtvCA8Acpjwg=="), /*88*/
					decoder.decode("/ac4ttjIsB8TqyfEFbQGO8+QOG62SA4/aoRVLVL18q7L7e+BZtrm9ieBsj/qfLl/1Ajn8odiedHUlBmpmsP1fg=="), /*89*/
					decoder.decode("bSe250vp4n5apbe7yPVqscVs0aKgontTkClAzutjPEu3Hqy3EF0n/3dQrMGUC5xoeVU4Fe1rLE/gikYrZ215sQ=="), /*8A*/
					decoder.decode("urSp+wY2YaDVxdcIIyuv/XmTcPaDk2GDZ+vR2g7EC9IWnHRS/+fjbGbQLbmaJwA7xEdVBbydTXf1BbajaaV94w=="), /*8B*/
					decoder.decode("dJAyELYfl5tLc7x8rX0k6nbkCf1HocbT4Tw9euxWmBzPdFQhdzOrS1xxx2hQaJyQkwITpv32J5bL+O8cKvkNiw=="), /*8C*/
					decoder.decode("4dNvYr9NszT2RTiE46TH81HBVxWuznp/3k2NzInJd+HiaJVBUv0nGIIqxFCgJQjFUfoTR4tHuisgCzOGtr8Hfw=="), /*8D*/
					decoder.decode("qoZZycbvWsTYy6c+u1WIqirn7MI1hkeSJw+IOw8wbEILL0FmhXs+NTv++Haq4cSd6P+oltjq/xPx4b8AmF0Otg=="), /*8E*/
					decoder.decode("q29oYrZZBhdmdKOvWNJm1bNdpwXEp8fmM6zlxOTZx6FxZDNUWIrrle/Nj2bDuHZ9Sg/kdzVteWUJu+ds+Nd45w=="), /*8F*/
					decoder.decode("g/RaQv29h03AFeT60HK65TEBkxsKdOLJQ1Le7nGWv1paWfLQpbsQM48SV3J1YlsKyfQhb70LAT4BSWax1Dqhvg=="), /*90*/
					decoder.decode("NyrlNL2MMAkEffBw0GURIdAWcYQV7N7oG1/MxOMI7EtJL+wiH87Y7EuLpFVoQ85FGwNcrbE6R1pYSQ6P9sr8bA=="), /*91*/
					decoder.decode("/WdDCQaNVEMgHkvsFrnbGl3bc8cIP/Zv4yMGJeXy7RNofRFFKr5Qw1Hlo/YXGeKZDTNdGQGX41/SX50idXyM9g=="), /*92*/
					decoder.decode("9Imd2JnaNvYIcmw+/euKhfNqnN3ExzoBWVISDd1D7YvxAVpP55Tpw8V6zLZUzCBh6XCHgrs3hX7Al+lhdJbYwQ=="), /*93*/
					decoder.decode("fAJ0pJUfn+CmTzaBSdGvDg2nI1GYICMpZ6PbYC0tsEix4LpMltWnHzCPwdxDyb9AauNEe+Bw0dFPmALhbcHnaw=="), /*94*/
					decoder.decode("aaH8lVIE9CBdSgxq5m0lkTRqwr7LI/+KEgEq8w5LllXdAacP0lZLssh70BO5r7U3lqaMiRh8eTNODp6RbPCq4A=="), /*95*/
					decoder.decode("JtVTxsSZ/zRh8Z+pLMdwE+4BGvJbiCwde03/e1WFA6gmX2GuGQ9HSltXtJLzvHyQD2Uk9eZ903PotRyclKYGIg=="), /*96*/
					decoder.decode("Xwm+wKu/bbXGefmks9BtRQdsXZpFOE3JZvc5ESgnSU9OaKKEbly6DDR+D6LY/7Cp/U4tHbEVQm2Hil2AzkWo2Q=="), /*97*/
					decoder.decode("tXx1qiDeaI7S4dgUvbQv+UCHdo0t0EG2NBS2aiwffC2+t1XT447pkZbtEgQAH3AF1MUiYfZm3Zm40ual0HCZlg=="), /*98*/
					decoder.decode("S9+I77hbiBs13oxD+VE1AdmjJZCSk0hZDTzAjzYPv8qT5TXCkOLKnwcrK/w72cAu0FB6eUjDGHZ7iDx/J+885Q=="), /*99*/
					decoder.decode("ZOc6/hPnefyGWOhVUR0U0qNBUPMV6tTVFosiFhHyJopE4/teEgxQIoJ6TDz7zudGx2lP3a5HWTyVvnb/vXeg5A=="), /*9A*/
					decoder.decode("KxwO/RmesTvVYhuM/XhSGNJ4/DfHIDHkb/eNZrRdp3aNgOYSD1Jt7Kvw15EDZwzU/nE3weH+yNthI0QVRyyD+A=="), /*9B*/
					decoder.decode("0EpNuTdc/B3OoeizA2Ifatdu/ri/B87SR8hIF7h5VdUuoOuxNS4qkWkC5AWj/qUwcXy7tQkYf41nwbwNTcvFag=="), /*9C*/
					decoder.decode("/N65DPKp7JNTf1fagiiA+oQVmBbCPBwayTPxnlx874D1nPEvF8UM+1Aq4du00/frASQuNNoLLNiFWrj2nZ6GfQ=="), /*9D*/
					decoder.decode("IY09+9gJeUmnkLFRX5dFsGh1CI1QTBTWskDvIQFcXdzBdCjL/WlweX1/8eihPDJaGqpOqEs6k3Dw0EsVwplr3g=="), /*9E*/
					decoder.decode("eODUAsaBjECrIwX7O6JneIgrkDI2oyFRxo45ynvq5Meh7EmZacMBqP9yntwypSAgE/iDB9WWVvmcOBFksOJ8ug=="), /*9F*/
					decoder.decode("s3TlWuEsze31y2mH0tK+z0GgoMhp0H+Cc7po06C9iXHqWr2UJlJEKN3JtoAPfY37x2HPIe330kKTD5fcPptPNQ=="), /*A0*/
					decoder.decode("BDpjstjk1Du1b/+yn/C90QOFruM2pyFhTwgmHAHfkyjPyhEr6HOMHofRUM0fRLzzSUoMdX88o4mpN3NVr2XXSg=="), /*A1*/
					decoder.decode("+SLoeHjrJ20JHZu5LrThUOqmfiEd7HmnhL7aRYbY4f4zaNwNh9X5Mnp+0qTf23+7V2YhtD2pVfGcWugHTJ+tCg=="), /*A2*/
					decoder.decode("Bytk8F57ojT/im35P1lvkiwNZp5h1L0/7OUbncS7h9fwGZ16/dGzsdxNcLkTVWi3Ql8hipuOmknxYaLnB3FZqQ=="), /*A3*/
					decoder.decode("eAPHuermOW7vUnMu5jdqOzR9pnak4VGCe4Jt5BHc/5IFTwYjW0kX6/zPuKm3Rkll04vhJoI4xeSh9LzufujE9w=="), /*A4*/
					decoder.decode("bBsHg3RK18JiMQkyaa62XVMjVrlUi5wxFP7WUDPJq7Woyfqj2h7N/XbaaLFVggzTW/x90W/IGKIZTApGcElW3A=="), /*A5*/
					decoder.decode("KHtuRzrxbPnuy1EiWWOn5iAGmIjMw/ar9FEM1UrtypoepMdNwCVFYLXwn+U0AWWE5/pP5FAoqKuNhNPyqjVJvw=="), /*A6*/
					decoder.decode("FC8NxFBRQepvMtCPm13hTovHtlIHXxkNhXIlTNgh+mf+ln2PiZMaJzzm2HCxCFd9mwEBLGW3RdVaCyM4ovdScw=="), /*A7*/
					decoder.decode("39f4tj7cn44fwCLcSHxKi74pb5ASNmpnGPlk1I8XHEpAc7oDyYxxqo7r777QlTl1fqnMPwuxgBZuZoKXgshzhQ=="), /*A8*/
					decoder.decode("QwqQio+5KkbxpYhz1wDVLRlOSe0GxHCmCwg9MF7/OZ7vojcc5Ak45GPvTZpt3nEjRxxhXmklJWoZ9YjytwH9zg=="), /*A9*/
					decoder.decode("zdI2jqcRffCMdE3x/yxSPGsqMCSU2Nvb6flmoVamymkMjCYHJbYpUkCIHnCOKcLpuRPWsfMk+pOmMTq9wI8/jw=="), /*AA*/
					decoder.decode("XoVgHKnYEg3XEDKXRCH+x2dSyao47SH24xvyFR4meZNXXLgkMvEOWBe2DBhgl41XgtXlUPGe2bby9Gi6pWgiHw=="), /*AB*/
					decoder.decode("6oFCrp062WkRI1ABEqERizWdeLpSztgUTYc9F9wEVZyHlQ2Yo0XGy9jOTlzd3C4LNZibjGPimpDqGCWBPDYZSQ=="), /*AC*/
					decoder.decode("EjvNvu5FSgtaADedpfOoAziAFc30X/pbj+LsvGzomjjEDap3TpeQl4CH269+BwecvJATnuyN+q6HzgbrKjraQA=="), /*AD*/
					decoder.decode("RVag3PWB1kX9k2AE2pesYukeMH6XcLtViFxz/kD6LRAtnARGFBBrYThd55kyAQ+DLYSUdEy9aOdLAERQUTATnA=="), /*AE*/
					decoder.decode("2minyxWahCHBoCWBF09AK4s77+fkBhyzyMPQpmeIr+SQTz0B0pWnThrEneWqkf3Y6HnvqhG79sJgl40Mcd1wzQ=="), /*AF*/
					decoder.decode("E5Yh8WDwS9p3cNUXwaOdkWqm+xnrA30p1o3pEz8lirKycHjzecEvkrSK6K0u1TXLLOOhCtZOA8kQyHpRFi7Yug=="), /*B0*/
					decoder.decode("j4MybOLiWV08ttJy6VyKGLqxrIpnMux3C/94+X7tGSlRMpOJmqh9QOwcnkAta13QrvK7JIba3KwqbI3cn3nF2g=="), /*B1*/
					decoder.decode("sXFBLJfuoSXWU1dJ9Nh5CD8IMFyAyxluPkWES47Wgu8Zf7z+w5aHnCRoJ0wnDLN+6ByDrc/pDv6ifVKWZN3wLg=="), /*B2*/
					decoder.decode("r/5kbgOqKri1/CNj19rjd2G/382bRc+/WvWGs7b6dmkqLPiJdXmSrDxzCjzaTiFuoqLkY7j4Yoj6T0DsA3KSMQ=="), /*B3*/
					decoder.decode("fkNNJV1i8H1p2OpoomFNafb04adYcDQp7eee3lQ1Y06d51TqUPHVYQyVnxlqGrvZ8KBRVoTVHS6PxiID35X9xg=="), /*B4*/
					decoder.decode("mRpEXMaQ5sv52mko8dTGibZ8ZmB6p4gwp6y1ZKc/+X3F8QjlPWEn8pcyGJfYErHpqeyBIj8JKARhyt0E0PGerw=="), /*B5*/
					decoder.decode("PAV2BT3Samo+rFm3xk7P221y08z7lPK8YJoPR9/dxrFibmPgSEV9nXF0TNLId+XWsg8JHI1HR2csKvD9S10IUw=="), /*B6*/
					decoder.decode("c1iL7vk3tXcjEFSkb2oAE4/E3kkYs2RAsR8CUr4+xE/Qm4kegEMIH9k/VIFoNKdolMj4wrmh9VErxLieCyGWgw=="), /*B7*/
					decoder.decode("6CT4wh+s5xOG9ph3DhMS6DwjAig1nU3f/DbXb5gaI+d4XnTXvbwNG3VyNTfqo3ScPNkdKVDPKCHasUXbJjRh7w=="), /*B8*/
					decoder.decode("0o7Awcf/vHL9gKXiVV3I3nqjCbzYC/adA5ftV3Ek7Zk44P7cnUf+dZBTr+KFNgI8Yqg6VHfrxNQQTmx9MbWbKA=="), /*B9*/
					decoder.decode("8nsrCNmsrz2IjNLoA6sMJNybZhzg+7T4ulK7shhl4NF7QBN6vvXtV0vLcc6CJjWvQk3sTzdEtgYeuBGyJcQLJA=="), /*BA*/
					decoder.decode("JDSy/dAqEN3eLprWgOAFcdavGjVM3uN+xPAydnGLJxZZF65J9gQ9RB1AI/H1lwFJnr9yhhZhPJPCYPlH06F/OQ=="), /*BB*/
					decoder.decode("jConTYcBYgxRXfNY9qNMdIGMnbobwX810CdmA6l0yyKlVptX+6P/qtosiOyxETiZBG3B4yog2lc+gMxq16E3bg=="), /*BC*/
					decoder.decode("28UmEIhTH+saTWElx/5UcHYMW3wa4EJldRnW91VihIrM3RfxXJhXnf8EHSA9nAmvPDNH3l07CIlihOkyV3ecpQ=="), /*BD*/
					decoder.decode("BSXCRxFEpeGtNiPsNsU6p/p3EGobnuVDxsOJFVZfyOuirllk7nXif0usYilz4OTrrKIp78Z3MABrFJ7qlua4UQ=="), /*BE*/
					decoder.decode("SnAJwnk76p9IbRQ3rCa352+PL6Sj9D0yze5Z8QiTkQwq7rnua6Zx/FkXL24i1xnHqq4WIgStXpBzHoiLTuiDbg=="), /*BF*/
					decoder.decode("5Zm9ZfMyclw3ixbFpoIkg/+Y0dxG/dShlC95jy9ZO4mxSOH88gCzZ1/5mHohaniMRq/hJpI/LnmNTTIZKwtrEg=="), /*C0*/
					decoder.decode("t/ZyHCotEZarZdX4BMIzevALtSRKDJjDMTRCtaq7rHJu5T8+ku5TY7v2kR2b0qHVpdSrTKA4t7HEwn2YkC3xCA=="), /*C1*/
					decoder.decode("xgt0VBR3OTnO1eEQgOOvW+RTqYoEO/btmt1nb2nj5VSGO+iFbPvhXlTK4wwFyuSXor8jKhvDqJRh2xM9pwA2mA=="), /*C2*/
					decoder.decode("vvqfvl0BolUvR7uGVjzixmVhDLnGDsSjrFWU1HYg//j0QSknwGikgW8Chk8anK7yiW64D6ZuEY4f5wUTIWbZLg=="), /*C3*/
					decoder.decode("2WD6d3U+bvwPlTnPdc+6HcF2UCoVrpCjFM0HpQt1qmncuwLInTlVBDjPqabHFSGt6pZYemCGP9a0cyEoZzvIIg=="), /*C4*/
					decoder.decode("E9EppZ8nJP/Xom1mByDMDbCMxtG6ND7N6SneYmBcaDMyMqE8GMYCKs9DSSoWp0SakgYmItfJetbk8cAFQ8Ls9Q=="), /*C5*/
					decoder.decode("RsqZkZZLwq5YJTijz6kUAFNUmLhgYIMo66jU75SYXaAxu5aszNPjQFS3cvqwKhb81F7bvaYgFvhBZr2fBcc6LA=="), /*C6*/
					decoder.decode("4D2lxH0NKNTTRmpuR0P715nB8wAskQ7coWe8p5QC71Qlp4Pu5I6SsZLSBcrISJhbreiadPnWZULPUcZGFs81wQ=="), /*C7*/
					decoder.decode("rZtec17nd0Ictg4htWh046wENWxdh6r6inWVIjGlxIibCqfM5xi2h5FmlEBmWNdWc49qtzmxd8c0V3GMrycmqA=="), /*C8*/
					decoder.decode("UFMI+3yHZVS5p09cU+dthlcLexExSE1JzX1P2bUd7qEvTHKEgiscBjcHqN4kFznzzeXma1U7aaCuEGOvZljXdQ=="), /*C9*/
					decoder.decode("QHrj/acIlhs69rs20TvviTIsmvrLnLC1h7LoEfAyISSz7nXPXrfcWbW/NRzKxEAVSOvi1MJy21299yWFSP7kOQ=="), /*CA*/
					decoder.decode("UKB8HfoSor2SrhS4mpk6ywNidHWW9gP3avEu9kua8bfqE06fF11gGrwuB1fH4h9ds8N4hGrAwOM5h6oKtV9mhQ=="), /*CB*/
					decoder.decode("mzEsy9SIB2KI18yKu6M7CDyinbWRFOeczXg+Z8iuumcDp/K7kk/AQU6xaz/c3eMUETqp0iJsxYjdnaTks1Xycw=="), /*CC*/
					decoder.decode("LbtxO3U0gUETAnC6raGxEUP8W8rFATCpc5NQfcQyyj0YmnOXitB/JzsLUDI3v++QTX1Sz1LdtE42PSDJncqqTw=="), /*CD*/
					decoder.decode("N+gcIiHfYTcJb5vatp7OYctdXergPaqaGhkGQ4fDQKac7wtf1GHEP4LdaMT76163hnP262Oamekqnh3HEfeedA=="), /*CE*/
					decoder.decode("xciHtoQ+eGdfmhC7ygIF5Fbb46mBd1VYnzINn6P+eHxea25xbRjXHp+S/rFZgt067hmm+PLwGWBThe0153BHMQ=="), /*CF*/
					decoder.decode("3PBNxwkq2sIUzJ/bZmg66r0BjTxBoDH/F6GdM1FFSMfwlIQDaS/ep1Td6p6s/TCnu8SB3PPHZM+q7TxDyktajw=="), /*D0*/
					decoder.decode("bxBvzgY//ewIfJN218DfN0cqME+ox6HtYIRBasfUoV02KgWNOV/hhavaeL32IV2IkqQiyow75Cdw6Cxb+az3ng=="), /*D1*/
					decoder.decode("zo+5ADNWeFT1c5+vtYmtTcZVlQFvchmi+D+BXr7IJH3nf2m4NEfIlGzgaVtSFQmXPd6OAuNBjdbrxtJO4gnRIw=="), /*D2*/
					decoder.decode("C0DLa8xl4pHC8GNllY3qFeFKgzax3wm0pBbK3fe9L8ugevMuxvuHpcaxVMpZZ1TM92aJV2ebb/ejt0V1GKUyag=="), /*D3*/
					decoder.decode("8t+QubRbHyfD9ncNdg7114Vc+ItM3f8xx0EuU16agFFJo/Tt63aVgeboR5RC7U9kVM77MgTXkKYVPS1IF2xStA=="), /*D4*/
					decoder.decode("13J82Y/OfrGLonb5kXaFonUyrReNb5W8ZBzd+t/QnBju0YgVaAgM7azc9VagJy7RMOXDWCJgn+GUutpLLgq88g=="), /*D5*/
					decoder.decode("pW0lqMcms9Ue8L/dAz60Tev2Gzj+d24Vywa0zJEGakuVToUaweZ5Atq+f1WYyDQI2t76ukhMxvmk/EJYGJ+OxA=="), /*D6*/
					decoder.decode("rDliPYs8LSZhSYvXSQTZB24EiNvP6Brfuc/tjSiSnavs1NLI61f5jEtxUb2I2QE+c9JNEgwN2BS6PjVxZHr2Qw=="), /*D7*/
					decoder.decode("5f06XoVyqWgtc42mmG34ESKbN2oFblmczKcd6sGgbDITfJd8GHH/60NVib1Bg5I0wLbF7dd3/rrbtd6lqMh+fQ=="), /*D8*/
					decoder.decode("W1NE+fbXufFEhfhc7Y7gm3T7lYTQhRDYr9USktrSIUU+K20xLLTbzd6tLjvkYPW2vOCOwQ2uj2Pemp9c3tWEIA=="), /*D9*/
					decoder.decode("6qSyMQsmLoCZEguhUFI3+tl6O0LgSFrJJew9DJINMAgs9WGq3Xzvp4Zd/X41lZ+2Jb6YIR20SzIqjyDkmUPIgA=="), /*DA*/
					decoder.decode("oPDaWgtOrJ8ccTqNQZt/LsH4Kkf0Rzldpg4Lo/9hFPjT1omliYc5mGpUQ9dekvjj3EdnAl6/jwnvShCqvd2nJA=="), /*DB*/
					decoder.decode("GEmAt4EqGZG8SWZe9hTIRBDxfWY5p1qyvpSiVbhSnzX6tDTFkvyedai527WGRporEXh3C1YWkrE3yb8eSKKeRA=="), /*DC*/
					decoder.decode("+Enq163LqECwKMe9QR1cuDD6h0ncJDt99oFsLDx3Y5xif/m7tvTQ3NoXbpA928GbxHALYPpwUq4ASJ3R7iX5bw=="), /*DD*/
					decoder.decode("tNIJHsREmbMBI+vu2UaYq1y+lgcjYuvIUGQbzQv/AAnBjbMb3q0RJ6E/ubCvMtQ7e2YF6EWgUNpp2pd7YZZmAw=="), /*DE*/
					decoder.decode("PltWeg6UFON9GEbOZSEGwFdqsb4XUHoHFh4X0gKwgS1ys9L3eeItfUE4W98r9f9XuwyLLmMn7LWMhTkCNjQ5Yg=="), /*DF*/
					decoder.decode("b8Es/3vzCtqpmqlBgC4+bD5EjCZKWm/69/zDzefiSKT8RbQVrYiveKnXuQhG32nVGTqm1nXn7jx3ePbhPdJ3XA=="), /*E0*/
					decoder.decode("UWLjTTLVP0lk+Ls37xSWqKAmNx/1o6cAD28PXfD2ZHh+0wufCh5mzqoidsCW8DqGeFUJCf9BYtUwSSq2Ivvqpg=="), /*E1*/
					decoder.decode("ggbvBTHcDeA6VU070neN/5P7XiuQ04HMPZsOuXuaYkjiUhN1ktohmz53oNmDSBNxbfqg4yaWfGcIUQQf6yDs0A=="), /*E2*/
					decoder.decode("vn6blmoJ57JIhFudeLNno0km1oHeZ2k6QJf3/3W0yl05y8Geav6fR88cgGTlxSw+ZqALOTvTuZpTR7+2b0vhEQ=="), /*E3*/
					decoder.decode("QPORnBIf48GOO/6dstm3WMh1w4R0MgERV3SA0Cl1IIaeh/LEbnGJydsVkU7j1UNPB9XF0Devx4ZUJwjUvBsKZQ=="), /*E4*/
					decoder.decode("Yw9BLb9ZppKameNOnr2azv1Swc2kxusS8NDLo1Mi7aMRE23zSfgBbS7Zl5D826MQBck74To5LeF87O2Ryy7H4g=="), /*E5*/
					decoder.decode("HZmNR2laqyFpjaHGup6RHLUJof9+UmfX8Zf5LRDpxl6DqQKThjjjFa9H3pGhkHrPCUOcevH9l7MJixHk5Kcu2g=="), /*E6*/
					decoder.decode("kEXLHevzDvOXQR6EkhYbURrlFOZqE9h5wf3egum4hkATYp1IADJ6WsxxEXBHRelxSIQbnuIYAtKcuKxIsxhHDg=="), /*E7*/
					decoder.decode("UVXGb45+hTlyKRl/mAsxQ8v0GxHgSurTHZzM5kR0x+zqqfAcuY4Ad/ZkGBY7udTKIbNVR0OEZ/WJVrjY+i3JTA=="), /*E8*/
					decoder.decode("kX//MekDIkUewM6KTUk7up7ifcp0fCNw3HnoHR29T6TYfEY0XYXJNSlzJKJCpfbeXNI9fZeDr8jCILIF5oaflg=="), /*E9*/
					decoder.decode("rOPm5sq0kYHlXFGaxOE7akM44dmnko0kxTVrqKwpRGnnfVx3qRlRTG7YDviImPCRKbNAj+iEFCx98ecKK/zIdw=="), /*EA*/
					decoder.decode("LJ3F6s8LJ9K49YRE3MkSSyLU6GNhGIBMYfM04cKpAxzVy5nFbfXkLnFvQon/l0sAo/bzB6sMzTzjNVZ0PhVG9Q=="), /*EB*/
					decoder.decode("dFH3TccJqFKdGtWpvnDwZ8dfnvCOLptmbzwk674j2KuhS05EgqKZedeU8W7rehXiTkBt3V0coRJ7y3aDHhft5A=="), /*EC*/
					decoder.decode("l4svN2AMp8hyO1mvKAFci4hSKXhn7ZfbeY4yodLtLFEL+dX/9PMoCE2GThd98/S436A434MbVESWEBI1QTphZg=="), /*ED*/
					decoder.decode("QyffOfPDRgLsx2v+9pab151cPvEdvJRgReWfsmCp7kanhPo4B9XLwz0LWMHCH2COhhGM207xy5WzL/ofWN7ZWw=="), /*EE*/
					decoder.decode("c08JmeaD6Opafr1hqLGrfoKNXLXRJLSk5RBTplq11RzdW52t8ApBtfuBIBT6R01YOltZV1zUYvcd+SYs45yKJw=="), /*EF*/
					decoder.decode("2gSNCzfgib80+yabHZr18L6Hl6FzRD7J+/rnE+a0rcTpvMHOUxL4C4xNBe8H6ntC+XHlXL+xn41IWVobgMXcfQ=="), /*F0*/
					decoder.decode("TXxrEE5mRZnuum5qHFMt5MrPAhmwGaQChSkBGSYXP64dJ82Vvc/e+7qpZbijrDHMB02SHXHWhZZl9S/V47LxlA=="), /*F1*/
					decoder.decode("GgmdkuZ5BF1G14IidZ98KWWHiqzdNxWFRW9AXHpAFzuO0ReQrflYeVtr/uVanHfecYchhxFxNP46+fo7bXCi2Q=="), /*F2*/
					decoder.decode("PVIrYrEyJjXrFs6aRP9liL2ZOO4pMB3mYjsPAAbviDov+J4k9Hn+zYylQTtuD+a+rEhYRiaQhNXMr/pl3XIqjw=="), /*F3*/
					decoder.decode("AT7KGlQwgcsCg6kZ5HNAozJ04pPanv58q9uMCs+XVMb3WG60QZZHrC2XCeyhx5WyAUw+UPgl4r/ZR1HysqVgyw=="), /*F4*/
					decoder.decode("bmEbbWJkTBxbIQuRS5iatoj1vj92DhB6FbF1mjcSR2ub7SB88SOvU0mBdyhIB8sfAzKBYYNy5yhNIIwKTR4QqQ=="), /*F5*/
					decoder.decode("R5NmZiOwow6p3+7lRilcANe04nDbAdGCZXNMcqcar+OOLy1NoA8WIdSoDUxz9YlKODeCfWU+xu0aW//qSFIh4g=="), /*F6*/
					decoder.decode("vNCLfFZ6yGmIFBg11KflwuBNLzcB6ynB8Tth62IQ2lv0byYQYWHpliLrUtJqtpglFpEZXo5Db1wU1TMpAMMdxQ=="), /*F7*/
					decoder.decode("JbzWhA3FNzJBoMgI9gH3I3Z5VRMsbtcSlgpyDTL9vKC7arebLbA0nCRXPQvNTobAwe8swFbG5uYAzJJhyDZN/w=="), /*F8*/
					decoder.decode("cjdLStvvtZx9y+EdzEXDNTnfDnvlPNAOzRSRVDeDRh/eLcvKsjwEUg32gB924qf/YAfB4kcICb9I0+j9h4sG9w=="), /*F9*/
					decoder.decode("Kd5dHI8WNe7GhZsgMsfpvh/uQPyq/OUoTGINhYSDb6HN5Uu6ZnLY+LtEhiPgqYQxGPMYA/JpWzvx+MD2/enrEg=="), /*FA*/
					decoder.decode("UUs7sloJ0beADwIapXy2dhjf3QTaMO4HMe6vdVvwEofpDMBIPZqnsHrlzZRKrpG0SuLIDw4FBE2T3fqeYs1kEw=="), /*FB*/
					decoder.decode("y7XWzc3y+rHVu8+8QClqdy6so4eJ1lJRROCM9jmJTKt9qE3FrsNg7yCzYltpipRf3lCrSK4NmYns48GXADnAjQ=="), /*FC*/
					decoder.decode("pCmTzCOJ2+hXx9MWfoDVKf8zp3/1GU4Hx/hC1DalQBj6de2yRNRL29PC7b79LJwlQMPudri7m0KAInwn9bjRSA=="), /*FD*/
					decoder.decode("QDUaOcYUo+I2Unw+XWuvVeSe+HcLjGq26UBBBTS1yoUGFqisc5tE0nBL4MitchfFLBzQ6rm9fK1KJ5d3WRJt+w=="), /*FE*/
					decoder.decode("BJibUbHj/vwmM3QlFwZb1iR4a6QWSggTvSmANWfOAxRaCi1uUflexHBaD6WBK806dfE8CPXnzBitfaCkW0i8LQ==")  /*FF*/
				},
				{
					decoder.decode("RNX+x+Ttav0JeDvRyoIe+0wKoGNrssznIgskrkqlkNC2FexOyVFwujohqy4N0RDLlglnWNqAjA3jAwFATiGpIA=="), /*00*/
					decoder.decode("t8YMUKBWqNy5q/5nOoN+DrMnPFV0hEyuoNID8lECnWhNHLt8ODPRMkfNwecehqU2KUg8GbQROSXvJ9LObFQhcQ=="), /*01*/
					decoder.decode("4QZz+cBSyNRWxNgEPHTCzU4awq6YCHnL/VT5MRuRXOxgQ0lPJnw3e9i+iLyQeXoNwV+7p39aX3+USC5/G0xayQ=="), /*02*/
					decoder.decode("APVtP0n+tPnWFjsAyuvlPyCQ4SO6rL4QxMd34r+EANTlF8qABuxB5jhKHQnWk27ouxLy1VRHP+VCw7HvAAv7ug=="), /*03*/
					decoder.decode("aY2pPfWYQz7QPxZL6GHXfQLNjCxkKypsn+6XuuH3lPEUzsb/O9o+b6brYvT+h4BOc635wN427Hiq+w0+XAdi8Q=="), /*04*/
					decoder.decode("PBO5pDNz30iekA1C7vbcN/9w6h3nYT+c0xqjJ9YjIX4ZovYKDGnFRmdTTa2dACEgDo0aFMjuaxz9hEdq4ewLAw=="), /*05*/
					decoder.decode("3lWJ2kAi2PIiLzbtYFbPjVAob/FJskWx+LOXJDqt3OC3+uvVH2hBJlYuWnHxtiLb+Tc7OhZcaad30CpRpfO4zw=="), /*06*/
					decoder.decode("WT/GLSQAU9nnFQ2Lnrf//hWT+PxJyKDNJ09YgnkPK6q9JPcawXwcjIYwKpzBnhnUafIFD0JueKauaNSbmKbKKg=="), /*07*/
					decoder.decode("TP9HKj0MlKLG4RjJeEkMsVtMM8tDJUXOa5a04f+M6FerD8ZtgE3+Sy+lKKfbsQKcx2/fyYilbsl4tUip2bjCvA=="), /*08*/
					decoder.decode("tr2Qcd0rREAqrle/2J60Jn+niTPsq5ebtMS9fZUt2/oC4S0hT1oG68eupVAeZGHEhZPVcVRBwuUM9bK7KVWeXw=="), /*09*/
					decoder.decode("wIebbvasA4DLDz3eP3/r6KYOSwCeW7b9gKLu8HHmxf7QFFoVt20Fcr5FjYDFvct4vNWG/2N80EtIgEoIa8nl4A=="), /*0A*/
					decoder.decode("H3LVrOCRaNJ5D+q54Vnj/QCkN69gTUeZBJouBzxQp5aRMA512R1i8n5hvw/3eINsVNuYwI5FKrJBB7Eua2+aBQ=="), /*0B*/
					decoder.decode("4OEMiBtlJgOaEjCzNOlxvJ5RLV6CBHhNN6843DzxHZBh967PoXKOwvf9qdUZyDujieNwxyUIYhRuYkxNRKOF0w=="), /*0C*/
					decoder.decode("mxvnXJSqxgYR3FKb+M1xqZWOeDzv0eQ1DsrIYo6vBHVN4+9jmoKF80g+4Dk9ZF2nkWXguO4J6OhbnUoPwnoDsA=="), /*0D*/
					decoder.decode("Ro5YYTXGeI3NoBAOXkJseRLqmMsoF9sbf2ua5IBJ9v4KHgk7nBoimDRCH+z2+Aw4JZ7SiF2qIB5eqXSsEqWagg=="), /*0E*/
					decoder.decode("nUy6LJ68R+fWm2l16qFDASVgUMbQMRIaDHtWHbWkXYp5DO1FPWyvz1eu056K6qhLpgs2xUkFQrEX2DLBl+LuBA=="), /*0F*/
					decoder.decode("P8REPuy0Khmrc0RtHdgrSonwbDYJ9qCe9eVyLSvet0HelxRORELo6Yij9RfRUTKaiouHSCVJkFgo6W2aZxoBAw=="), /*10*/
					decoder.decode("kmDf10Hfkw5+LKutolYOpeaPKMKitIwmH/VdvmsMgCR0+VbVfosNFBV01DsIvFmIkrqFZGChR8jYlWeb45VdvA=="), /*11*/
					decoder.decode("SCTE0mPdcalr+PBRZ0fDmuX+RBMG6/qklIL7UITf8jFdPtoWehLdRKKPYQ7KDMha1CVr3be6MetnQLNm5mWv1w=="), /*12*/
					decoder.decode("xACChW1ewU6wIJG48Z6axD7rZvZzQVMditQ3AreYswmDQJ2y9+rcVTLnFNIyazDrn7e3xaYQDmxzcgqX0hSwtw=="), /*13*/
					decoder.decode("n3G/5K0e401H4NgBIGQmUd++dHaPP2VEYKEx0eXv2+Li8L8/rQVqY4F36RhBbHyMaPRVMWvKFSru/xMO3qgB7w=="), /*14*/
					decoder.decode("MFFY4h8qw5yuMkfDoFMfzTpVEGqI8dNJ4EnFXl4wIVaNoX0RvN28Bbxc5JLFsPeAQEFP6hGMSLBn1cs28iIgbA=="), /*15*/
					decoder.decode("+yGU/qUniY/CcqitemGN0IPWky79bSvwX7ITnMa1kCvnGjIIRaJIOv4TW9dV8+UuEIxoaVpNG/YutOqpldMgNA=="), /*16*/
					decoder.decode("ZFfsFvqF/8SPkKU70O4xv7/AjudKQFERf/F4JhuyxpdLJQqq+CAsLBy8U9bYV10oxinfYmPijwDwjL9JSWHTdA=="), /*17*/
					decoder.decode("Pw5lJUEsKeNyfYnMm17y5kMWekxg/TivuNpK4gAyOUmFcxvAH1AtnWH7TnbLoZaypEnR9pUtcxNUl2H5qZTIhw=="), /*18*/
					decoder.decode("zXGxgJxhqHKsLIdsbzk2eERmjR5agdNXHkh1c8/od7wKhrsqsa28nCwetd49oEH1A82+u/V2sVcR8zv6mGni5Q=="), /*19*/
					decoder.decode("RRsAddsDWRRKycGtVAHjQkyiL7EUU5FvaD4EaS07QNfxvq81Oo40O4uocUsxGk7UiqS03ZEsQZLVG5Of8Q9gzQ=="), /*1A*/
					decoder.decode("7aQcVVpqeCL2SSS/WmJYMs9YajP8h4McWoN46rc2EqyC+YGNwq+SnzlR+jT3x7JW2lcMmws3jG317V8Dxg5Viw=="), /*1B*/
					decoder.decode("0qnj6a0sZfBWi5/QhRjqcdrIqKkOvU9U4VlFmeUy0OZFIJzVSZ8ULpqWHPKagYtzLWt3PV8ft3mIa9d1hdycCA=="), /*1C*/
					decoder.decode("AAG8O1cx9EuqXO7qbCEsioATT1Kur3r0OQKMmqOfRw5AoVZN0+rby2BkF+IdXgAdMoLyEkTPGzPh2oUUhb7DAw=="), /*1D*/
					decoder.decode("65SsszJ3U/tWvfZAUhtVhl7KmYHhufGFEWWj7hch1b1OIvLKJOhb+Yjytd0SWd++h/KR7XITgAELe72z9zqm9A=="), /*1E*/
					decoder.decode("dCP2TBK2Pu+ZpL8HJB3/cc2XeLLi4AIHRfiQXANQB7/2fzCuJueDyBNmvsAxKj0NLjid+BTbr+kt2pNZMdrpIw=="), /*1F*/
					decoder.decode("o6c0T020Xc/Mm5d2j7UNcsKAd7R0MDkGGOtmtTgh49ZAlITyNLHwVm1zOtA6Qn04ASaWo4uuFlRwdHlQiBxFKQ=="), /*20*/
					decoder.decode("/VER+CaQfQYVS5OxJeVeG1MpGHiOU9c2khjQLSFQNzml9glR0CXIRrjdR/4oeT1PGWOP/d86FIGN9gXeTIdVVA=="), /*21*/
					decoder.decode("VWgkAyD3ve8YVULiiChTYr+54+xX3VntXofNAFcvyP4xWEKZSuaWa3D/FzDH/AXtnHWsRIgmvmeaprTR9vsduA=="), /*22*/
					decoder.decode("StVO8GtXzDL770enjMkF47YRsqEm+KIoO1S/SUb1jmc2IdbI6d62XfkXkNdIN2n7MqbMS1xmEI88ZtNuOjDWDA=="), /*23*/
					decoder.decode("NDsU9vU8DJR9Y+3NazNIF2t1TvFa+Na0i7L2C05gEBfyAR4BLaKvwhXCggrz+lbbTLfLB/Mmn9GV5qYgCv1ZSw=="), /*24*/
					decoder.decode("+Tvv1VMLOp2HLfx/xV2I8uhRNNgwCjv10kHemmx+kZFsk/jiwxAOwhioqdj6hnWfyPTljJDDJ39TeYnhfRYCUg=="), /*25*/
					decoder.decode("ULz2ZMOgwg+ZYI7dk5+ltvJcXe5KN5IiSxmxVy2dJG5XfWBK4wI21W/MmftHYCHGdMr/H4x0/jvZ9PewCHud0Q=="), /*26*/
					decoder.decode("8CkGwF/e0Aj0385puXoC/J3RqPbfDTw8vWllTpXYKYPupIA1vE1uaB/FGQxewxO0/ZZpufBdZ7vwtP7xF2v0Ww=="), /*27*/
					decoder.decode("nZ3i/BigluRgxkleLVnj3vApjYedO1OVu4yU3zRdiOXNnNdFPWbm63RRQCj/DGzcDdgS6V/6iGWeF4frVStLBw=="), /*28*/
					decoder.decode("MlgLBP5n4jVJNtuka9CE7O4V2wUtkHwL8RdqA7xKwHlh1wRW9KgnUWGdwM+X7goUvBgC0GnPfO54W+mNsaBGxg=="), /*29*/
					decoder.decode("J1KfEzia9JmMV7HLUaIoo5w2yTLcfPSVSRmEnSJd2YPEOc2poA6suvZPklRMKZbcC+efyiBcowdasnom2tTP7A=="), /*2A*/
					decoder.decode("C/Q1O3lf++iIKjUAvVMpFv0nRuP00yGrBDf/M8yq5nOe2F0EW+butjAdS+1OzQXkLwGkD1W0wMY03bhPE5Z77Q=="), /*2B*/
					decoder.decode("9xmmwTrViP+EEIdUwKVVwtSMzjQXbaWslccO09jzoI8CQHWgg7vnmM0vrSy4nTLef0clcTlWvstDOGxyocckDg=="), /*2C*/
					decoder.decode("eMyzxLEvKIVirl5IM5TP00YMWlhQ5LWaXN1jy8fBAJNPvT0L9BBf3Mf+UgM0gXV9GbEI/eZbNhSTLUSIU+4VgA=="), /*2D*/
					decoder.decode("QnYCccHhpxBz9K5WnYeVH+pXq4F6FHlxaHk+++xbjSGzRHj/Ig4QO6ZcLs+nOgZnSzjYoo5Lr8Wq4qmjjOa6fA=="), /*2E*/
					decoder.decode("TRjdmcj6dDAy9lQXyHgUwGSvsMznx+vQMdbea2Zc+/5UaukMbKCvP7JXRojV0JomeqHvBpIr0XjFhSVdIRW1zg=="), /*2F*/
					decoder.decode("gp8HMic93PxBNoFwriu84SNMn7LV3Htj43sgUYIfm+c77i4AbMESPHtWqgQ7HowPHRnkuDdUZWPVXRjVuK3z3w=="), /*30*/
					decoder.decode("282IR2Zd/7mguRnyRnRwzw57OVOX7OoOuuhP4j52Jg9kjC6hNgtTNYuTFEA1aphNiuIuNX0H5gvAl2u1fnITSQ=="), /*31*/
					decoder.decode("oN5eetpHgQHjLxLaY2itDe6tM//ddjr+BiZaV5+SS7sM8UDtieAYIMiaw2AMxN9POnO9bKXMHDtC3Sj1OCIMfA=="), /*32*/
					decoder.decode("FJ+mudPSh0xRfPmZSz25aIB7nYGlYIJxIv2f7OHYxTcG7Kgccd7p6hk8NUSL4SZ5Qkb3XEvU1I7lp/hSeZPbjQ=="), /*33*/
					decoder.decode("fUTTbi/qT6ASZ/UAU5qwbLdv908d9WMX5A20tNoIMIikFCBj7v/qXo00JWuunmi87KzSxuevP8YJhxY8xwPRtw=="), /*34*/
					decoder.decode("Lpiew4ljXd5n/15/3sf6YYpqVeTLPqlzdY0Bx7nOawl2R/HyEt+D7MvFVpixM/oEdnrQv/zbVXt0Q/KkICjaPA=="), /*35*/
					decoder.decode("49O6nTVjpBpaA7M1p45fZ8Sm9vgNPTOz0waqDXkvK8GoveiFHdBG5y6DXW+WI7vz5Ooni/3haImcuHn2/2+XPQ=="), /*36*/
					decoder.decode("iOvjG8fjl/RRKBCGnDn/JertZk8e2+4ISreeRTT3at1W5LeCUqUVipwtS1MfOVceYQ8dgkuLCXzcahijSCJaDQ=="), /*37*/
					decoder.decode("HQqBdIRapouW+xE3GtM3WUmT+YPF04bMPJAHUdq2UCwDpLCMCw26UDGeF3FiCwGEX7/UdIPlqFPBMtAh2HyuMw=="), /*38*/
					decoder.decode("KgjHsMmGfEREHB0UMsWUgDAW9yrC4TTYcy0TCGvhQASJxxJqWMWGNsbAevw43Rd69gLOIBlO+FM3TzDr2SaO+w=="), /*39*/
					decoder.decode("QRKk5b0GsX/Vcc45aUoju6eiOwmPDD530TD1d7836w2eCbkBtWHw0XMpBWwURT6KRV39+pUGjjjJLoFtitcIcg=="), /*3A*/
					decoder.decode("feg1DPZb4cLUCuFg2vuZqc+HWoTxtNmw1rjdk1rVdql2zU+Wtegqa2rXPGoJROaBdj8jHCWY8RjqB4mAV1fEcw=="), /*3B*/
					decoder.decode("fZRU73ZaL9dbfH0w7LFvnKGgl4n4px2mrQHKYdg8C48sz46rUIZk6O9MFjAAOBbA+yQpcU9YX+RUPBAmviZXGg=="), /*3C*/
					decoder.decode("TLz7mhy0l4gUoSTszHNQ7fXssTeo8tj0Q7VfsGJCN42cMVbjYMs7EeOlYShhj5WPzFF+qpoVB2BCeM407HWkOw=="), /*3D*/
					decoder.decode("qZ9QNnn1JzucnTQJU49vnSKTMO1IjbLStrq/wSs7gqpbYHF3KLiuJDUNoBsOsyiLkBJKuMNw54Vt/Sh3abKErQ=="), /*3E*/
					decoder.decode("x4GEH1qA06qVUbqDvejmOOeTlxk5xs+wpviBJZYiwDcQ5H6Km/OuGHWHjyHQqb7+ER4JAzf9tz4DSccrNn/DYQ=="), /*3F*/
					decoder.decode("eRo5vi8VETidIxzHMPw6hRV3HZWtK0Z99n3ZeZKle51Srsyu39ZgULFyH42KbckYLAhh3UMRXP1XVWtmKg2kRg=="), /*40*/
					decoder.decode("/2XDSddNg1u3UYhUAbXu6JD8kAbrckuatzmbtnafqRkJl4ztgi22cV5gN4ucRpJCUxIWprjQGdFcfBsx4Njv2Q=="), /*41*/
					decoder.decode("hETtIwJdCuMUDZAMD4Sr8gxr+59Gd8bmTVhmACUH/VjTgRMtRMN3+dZjCP8AtcoG37I+Y6ImRA7vLXjPQO69Gg=="), /*42*/
					decoder.decode("nmSwIgzi7ZH/QGzD+EyXouB6R0VzaIxpkroJBfcIGVvgI0UQSJJ7JIgoAalqTz01xFzCiEsEIFD7GSj1sQ5ZCw=="), /*43*/
					decoder.decode("eNkJg88/YGaf2lTkV2kt93Nvq5LnC0Pvh4J/NJVWNCzb+dg0HRvxthejCoqp4Do4MoC18Cptkleo0q8HhsxZfw=="), /*44*/
					decoder.decode("BQG0tC/QiMEF8skhkBikc7V9cIgIiTMqtglOsLdVy91fO8fehwL/jTNadI90m3ZgkvhBg9xfb1uPuk+2C0ULpw=="), /*45*/
					decoder.decode("kN0BoM9p1tIsVY7rw+zYffTUM0KkXYTgVjmwEFNHV/aj7oHfms7T38SWTxs+bWSWHKV7+SGOzbEEA1xeN0Z1kw=="), /*46*/
					decoder.decode("f93182kpNVflwhTGCNEbTKS2J8sSwWxT2BCCfMV4hrDmobsWNuOT4CojxH2HMqFLn4fK566T6rU17EaudyhoKw=="), /*47*/
					decoder.decode("va6Dm4RUklF4xF64/bw6AWJMwX8cUIxnEuQ6io4kQU0FYJ2GmRMocXJtdOVMscpwolzIgUxMGYeNx61gxGVmvg=="), /*48*/
					decoder.decode("2BoXV6iqf9k9wvOMvJDAkC/Qo6Z4JyixsVyrziODh5I2WxdDihBIN5PCg0rtP4M9+thkQhuElWpIXJUrhi5n3w=="), /*49*/
					decoder.decode("2tonLzHUyTH9ZWAwp5H2ytZhROU+DTpvAzcfvi65vFX584KBx7nYCgnzfGiXL+5qYzvoNg/9cgr7z9d8cZUESA=="), /*4A*/
					decoder.decode("7moMC+kcepOLLcCtCRF8ndcWkuIukqdcoYWnzXaqxuEkTkbUYhE/xZKbdd6lUc3mddiumGubL7nTFE4+fv/k2Q=="), /*4B*/
					decoder.decode("ryG76pwZZp6T0ufdPp+aXtobFzZSReUEMze5OK8rCW/ZnhXV88OKEAH13Z0GBsDlIiX5OAIqUxQhCe7pogilbA=="), /*4C*/
					decoder.decode("pAO00yL0Z5FFqWPpttKcNdwrSnnA0eTL6Q9ZVuVBSHKySavhsvNm9YpzLuLXcFiXLwZBJuHMJ8YUP/3zjgKFwQ=="), /*4D*/
					decoder.decode("O4g673zYUoKZb8QfCIdLY/UDLCtlRJP5lfj5EwGzSsTKEGuscuRVcwMTfR+I27arbqjIRiiRozpxQyI9RWhvqA=="), /*4E*/
					decoder.decode("eX1R6dFuAjAc9kmozcW8T1ziUtcktaK03kUJJSdPlgrEE8+r6jCbORf5h9Une/a0IB7YIw6XpTs3NyvTQjfa9A=="), /*4F*/
					decoder.decode("bPNBgbodtlgtVA/JLC81Qehn4CEhWhXGnsaOeLfsMwJg4QjcZ3AHwKubOKMfjpuOnqIi854wke4W16fdz/7Aog=="), /*50*/
					decoder.decode("C2S09lTKl3f8zSq8VRhjymT+ar30umVsg7VGIduLsyRP5yr2HzXMKeZD2hiFWEyBD2oyJ5jEkNIrqKsHbyk/dQ=="), /*51*/
					decoder.decode("Pi6M+2gknyosC/FxPLer0P33rhCrIY0oiLDi3Mbi+rE6aqdt727kHcxx73qfkt+uuFizqZN0mJzbD8U088kSGw=="), /*52*/
					decoder.decode("IUfDtpUo1GbxValbkuxKV4YEVzxXgCdI0OXUlVR/1J+JIy0hBTgdhQnlvWuhnczQqAFVhbeDPbSjohhTuQy7pQ=="), /*53*/
					decoder.decode("YH9zKmsoQUkdKmGhM36u2tax/20fZjT6TOPhpoG/DA8mlz/1CkiqDUrKdoTSFBPCF9RiKNOngWccD/4narhYNA=="), /*54*/
					decoder.decode("OPeC0lIA6/uLA3YV1u2hRmju49250H2bKdrU3K52Eo1Ikjc3iwRIv/ocZ4QC8wnlNap3U+Ruh57LEJ/QFNzJew=="), /*55*/
					decoder.decode("EuLoz59p90RoMOuUxXh4Kyxy5Or6dkDZ9bgosQPT5I+QCMxLU1+0Tg6RYQU8Fn/9u16xpS1jIIKs9HTup7BdZg=="), /*56*/
					decoder.decode("2yqw5OPh1KFlQck2ZefMwGpzK+6wv8wPdQ73Hmmjk2knOCcnQYytXALECG/3yWDhtePy8kYr8B0uFrMRRAYzOg=="), /*57*/
					decoder.decode("RO5v9O5xWcNschUfBQ/bD6XU+Mx1lhQ0YeKwct9+wNiHfhEkfaXHelFeO4hM2fwB08y/r1zGMZXdRHix0r0+SQ=="), /*58*/
					decoder.decode("p5IpJm+w3rqrqX9MCFaUGABqzlaCxO8PTx/c/V6H/AZLTBLP/xgTkBNiLVKakotiyFO10vpt+9+/WrGVZhbvpA=="), /*59*/
					decoder.decode("lfKcqYUZsTAGucaUtch0mim5I+xWg9TFYRtzhgOFOTWQx3tsFZpFW7wD3T5+RtQLWazhwnnVuTCzGIb0Hgz/iQ=="), /*5A*/
					decoder.decode("cuqsRM6oQTjhY+I8lpQ+Ez/7TLKibCmRq+4+XRqWQzwn8QgEqsdmsPEJbP+j7NODa9iaqdcDMBMxFeXPlzXQYA=="), /*5B*/
					decoder.decode("K+/pqTL0P2LuuW9SSesFoFjG1zLKX3MHjJRcjUhamvDq8yBDYnZpESZqAZbhmFZFez/P4TSAeC+Q6VerqnLz0w=="), /*5C*/
					decoder.decode("QlPdjHzY6ubk1Jx1m9XVNZWhCN2znEkEkmwK/WpNTHsLcC3WYNVBbLjBsjUbKQ4E/lM0yMprAfGaXQzBPXBMpQ=="), /*5D*/
					decoder.decode("ogpRNhZ5PnGyi//rV2AmoD0bML+5O8ix+YauZ39MMrNVYgOhM8ULS5Whf6n7/FR4xBelbImY4iLqPH+OerX3PA=="), /*5E*/
					decoder.decode("2UpPqWeiwo/Av+1MsgAnkXMJA7ufPD1ju4zYSS7wT/P6h+O7+j/jEmsY17wx0UlvsAkxyOdhgx8kasiWYqU9rg=="), /*5F*/
					decoder.decode("h6jZlKgFZ16mghsifMhCRK5rwII/oLbKWk/akeos0t4ak5Sk4+T2AY+He2zlYNfM132FRSx+d7fPVVmwCsqQSA=="), /*60*/
					decoder.decode("04wjwLG/dtnK0Rvv8I1ZdbAYtoMkjjW/slfUvJbDr9HQrHAg549u7Zc80/dCnw8kUSpnnhb6UGFNyn6p/2J5Ug=="), /*61*/
					decoder.decode("6rrfCPZYx2+zE3sOibyeSxEILaWCUlLoVTjnTh7+2w6UcyohsfldiRBLKhhv76uhFl1nZFgsF4/clZeGNIq6dg=="), /*62*/
					decoder.decode("DQvLX2HPnfM3Thu1xAa1cZn0Y068KkK9l0oUK+jJQspa6lBx9GvM1OhOzuW2rtwWl6pjhCP3uhR+bBdT51RPZA=="), /*63*/
					decoder.decode("6KfKqEd1fPx1HA5ZpaQQEJJc25T6Eq3vaZyMf8qtBHEavVWP6Mgdlm1hVNPBT44DVXYMPRC1ZZZXA2VnR8N+TQ=="), /*64*/
					decoder.decode("h+VLFe70hvItpCQ7g+uaT3BdrgQNzpNgwIFkhIusGCc/6fA5XHhoFkPJAU+Sgtjrx+k0ZODetth9ZcRcgiRl+A=="), /*65*/
					decoder.decode("TuV7hiL+sb8aTReoWt2WvLeSZTrFQ68PVJRf7iq/wTzvcN97o8bYwa6tuEz+k4VtFAoEcyqoUbBiDmWIcX4fdg=="), /*66*/
					decoder.decode("JQtmlubDiJA9oqLNZXMEgitYaZWPzo86IHDR2CHPxxL4wPn/5RX5qVCOniweFSml0Cbo/AZv+gMZahQLOHo/nA=="), /*67*/
					decoder.decode("UTxDKsykc/puotJbzkhQ9i0XCuuOo+3U2HwyQtiAYF6t2PRMXiUF3hH3I8T0h8yLF1UMgj+jlYbfI75I3cIENw=="), /*68*/
					decoder.decode("/nwiHKbM/zwnys8j0l2uU1DgVAvWvO/66Auy05VPi3SUa0n4YbgO8lwaR7erIqSWsuyTaHrwltehmnO5jTk1Og=="), /*69*/
					decoder.decode("MSafSU23ms0PzNwGfNeXpmV0AEBvrcRP/PfJ00igOE5vv4eBuGvdKmibaZ5hJx8iqvPETSrYn7igqbGnOvs8EA=="), /*6A*/
					decoder.decode("1cjTWkj3NT7ZU6yv90wr4DkX1G4ZsdxdhmINivLS2t+9Rrrsl8ZIBm8uf+62KVtiTME1/z63DlItobB1BG5ruQ=="), /*6B*/
					decoder.decode("sbB85QMUtXMBqwi8/RO8J64mt6whi+K5c+mr6Jg9GRrpXG1JrVYFMUXiPsFK7F+sOyWfBsKs7puJjJ40t8MDfg=="), /*6C*/
					decoder.decode("u9W1I4WPDo0h7ylwXkQRK3uATWj9OLDgpXzZlU7K4b1x8YrT8U5BYRWCM+iiGpIADa2RmyMolK9G2NptdQ82AA=="), /*6D*/
					decoder.decode("siOTBpzbrG9uDJbSYxnhDveYk15zW7CSJquazAz0n+ac+VOREOy9TOpHmAzYzooxYZ8il5FCg6W3Ff4MXRJX5Q=="), /*6E*/
					decoder.decode("+o1qPiCfgm8p+aCj/Jarl9qBkRyFNv9sii3zqytJGY7RgRaLvAMbyD74aUl8XGqYtm+zzt1zrJaX5MRcrpl64Q=="), /*6F*/
					decoder.decode("HuQykRqR2FR6rCMJe2nPdCeySdePyRezuZeYIHS7+nOmTQDpqEqmsErDDxy1NpCxUXk4TpExwSRpKpvYnUwkYg=="), /*70*/
					decoder.decode("H+exlIE7YHqI8H/+1xHKO6pu7thrRy6moUaiOmobDx6PQgX+0GOjR5yoTAqK22jP1Xt5QtMVAuXuAUX/vdWaxg=="), /*71*/
					decoder.decode("25eff033Amj33Y+pgq3jkcncnFl1YuZRfvbvpqvbHMVjqKhMpWBqkATWIqIT9733soU3QqUBOGMqUgaeE0Urxg=="), /*72*/
					decoder.decode("nX/Vp0+54c427hAxcDc40qQx7aY1KGmUuRbgVwkh92GjKRJt1j4+JZnW8lJ3Qj14QYQoX2RjJsGJI4JF6/uPxg=="), /*73*/
					decoder.decode("un/8SFyX7A+LsvHLELbKNADYl5NIIh7gpErm06HCAmlTweiBd8LM/7ytf3azcmkWW8PMY62SRbCSa3ki4L+i1w=="), /*74*/
					decoder.decode("Lct4XV0iO7spAaFgKfIZG4GI9YsejNqOI7aNg+MWjXo+KOgUiqcQAoUQ9DmpjncSzi3/fh+8EMV/miTkC8mnsQ=="), /*75*/
					decoder.decode("UKWSTC+F3YZcVFuxLkWx/1pvIrARBK8VtIN0P3JYvhcaML2Y1aA08MGgljndHtAnVSD8S81SafAPgeaRFYvNng=="), /*76*/
					decoder.decode("owbuG+vqKAveosswZkVST82VW8HNKtck7rmpAMNyc9khtmD0jNF5sa45NhPT5Q5Zh9fU/zZhFbRPD0GVgwA0Ww=="), /*77*/
					decoder.decode("sDLet4/oFCu4Eg5khsPrPdeMW706h3JV6FNHtxY8TbH8vtdWat6/2xEwrmVaZfOVHaDJLx4qLO/++nz661oLSQ=="), /*78*/
					decoder.decode("UyswkovOLZWlHAAGXyUuydyh5z9rRFOvRrKR3S9qDYcWi+RWpB9VD2WJkLULV75yUvSPC4dixPIV4QzIt+alcA=="), /*79*/
					decoder.decode("hZ8Qi/aAWRPnymjLA7RxYceJeT8W5GQVRfRDyIOGeuSPvGDiX5tjvh64Vp5wOAH6TvhEwtWd2Ul20jjCMfW3hg=="), /*7A*/
					decoder.decode("aFhrdzpiyPfal+mrZzWZ3bh1325YiRyCz9ZKaIY9TiWusOk8V+ZYtCP3q2VNxCvKYHvih9Jxp/9JmL1VtgEXiQ=="), /*7B*/
					decoder.decode("DjAc/tKWOvr69JaAr5xEB7o9/T3C5OCb5i+Gc0wGEpPRNbDF3knFuqQa/kgL9XXzdRyo12GaWerINShy+/rQhg=="), /*7C*/
					decoder.decode("+sM6JuBmry5Ky6PWd0y5D6Wuvd6IJFjCE1YkUEDkaI8WY/yX2H4tfEKnSJL0+W/vAY2MtOyc+tV7T6H7tpuiug=="), /*7D*/
					decoder.decode("f5pEW2szIH0xHnhM8+DItSUu9R52Wxlrl0lKA9dz7GLQ2eoMmCzPxUtYEFsOq6+e7SFMZ+7Gv+mKT1v77xPxDQ=="), /*7E*/
					decoder.decode("dvQuM60bVoShmSmAmurb71nXFmi6t01CQa8zQop5aTxazbQ9T9KXyvRCPbJMOV33wvKWp+1uCmj53q1W2FUDFw=="), /*7F*/
					decoder.decode("gmGdLaNlfOlaVaxCtsJZyFpXDPsQ7qBCEsCa5NSEz2Aiy1d7/ZIply8H/GR0vLb0268/tphzrbT+JkZ6t+kcNA=="), /*80*/
					decoder.decode("V1aPndNDlU62CdrjlWpHtwQvpfwSqPQEHTWgGU6205LbHFNkr7P5J9oErBF6CjKmDwJ/uf9qUHp14Zx1WKXfeg=="), /*81*/
					decoder.decode("c6fMtmH2e+nRxNfTGIM5ZEErkwTLKRy5XirtEhWE5yWSNjezLTqT8vfcQlXi3IB19p060VjxFfEBaHorRZITSQ=="), /*82*/
					decoder.decode("sxqycJCJtcT2T9omW1fvOgvX0OLAKSSVSghVvQZX+Hh5JYJr/63Mgbsc6xUqxPVmxvioBsE91+78Co1CAPAcjg=="), /*83*/
					decoder.decode("NKwa9LHyYKXVAa0u0Z4dB1pcZJpZQ84zLuAj8vd6kC9iQUMX0lXpadj37V1/kAxTR+ih1nCPdaQGmZZ0a5p+gA=="), /*84*/
					decoder.decode("TIUeq95+4b8nKMDeny2zMRnm5HF8kD7IoJIu47EgbTdXpsDlJZrmFc9ypzdIa1Kp/y8/nkdCICn/q1oipK1p+A=="), /*85*/
					decoder.decode("3V4tz78fuUj6IJKkD77mFvpSvWtbXdsN6VO51EGs1Y68cQdkOx0jGb74g9G76ftNR7n0ADoh4FGPegDYeNBHLQ=="), /*86*/
					decoder.decode("QQUwyWkdV1egTxowkiZ+g1rO71eroJbNYaYGneFgNKLoHLqXa6ZsWmY27Mu9pkWkdt8hWaUNf0gik4GjpGpfYg=="), /*87*/
					decoder.decode("jUBKA7p82oEU1kmU1Q+hon2PAGTR5rzkjWGILZgJNBI7Fs3YVm3MT0IEBc14HwP5HdlTxdPD2uWhuM+MqN+PeQ=="), /*88*/
					decoder.decode("+6iBFA9cciJJdFiHnjh5XbWo8JbKc6NpW0PMV874FEk0ZgD3wKVSJxKgXkOLrrT/DoZZj41wP1OqsdLNAxAsaQ=="), /*89*/
					decoder.decode("adH9OJl8DvQY5daza27js1ex0RBnPVz18JbuFv1XFIp7+CPW9Jditpls7TRJHej8sSziEuZ4w+XUutKgsEruFA=="), /*8A*/
					decoder.decode("/0BLRTCLJ4M9qigyLLG836GZLFupb48DqG8VuISGeFHjV68in+xM0/vpWX1cIukxf1/PWixEqatJNILmWaJMvw=="), /*8B*/
					decoder.decode("BV100wIK4aNOR8XWeHnCnzstVFZ/bQCjZ+15EKOwJTfbGVZqt9XGi9flVNKSTcxzuX2EOTddcI90Lh1ShtucMA=="), /*8C*/
					decoder.decode("KwNcOxnRCYF0VOBMv2qsLQ4VjxRlCvfGtT7VwC3YNdS8vMsiAEpgZqBXqVd1vMkTA8/9KBvf5IEHAIFTQfA5Aw=="), /*8D*/
					decoder.decode("QC4QGCrNokYxePfoUq33XHmyMGKlzgyB6T3tGbDMysUsKtolEjFamklwJGbxXjOtN+lNszJPkKZSaS97KfVe6w=="), /*8E*/
					decoder.decode("zN3TnbXmjXnhaR/t/naJj08S/GF4L+DWQlTxVLCTKmWZKoy+bKTY9boL3UJqeEeOJWGoejDhYAgVJuaTCYt2Fw=="), /*8F*/
					decoder.decode("q92mNarEOF6q8bw0RPSBRAK9FGk4B/xgXIN9Shwz9nqIX+YSpwdeUPoEkjvrwTYhsyRJJpsZDNFXc+gJTC3rnQ=="), /*90*/
					decoder.decode("10p1nzlNVQpwt0OZzDSrDwKKjVKRZ7PbtQXuckBV6Yxpv2hAODSLPgXdau4PBPdDjeZ89n6Miq99wzgM+9r8Uw=="), /*91*/
					decoder.decode("w9cDVyLO+l0vL+hsE8/w0z+5NROFNQfbYpEM4P3R19gboYoqSqWPjoRqsGSi4zbbrGcCGf05QDgpAeh1C8usXA=="), /*92*/
					decoder.decode("i1nwmUF1UIXJvQd8ILh2POsG1gk/PiJsEC5Sv8teM5OvT/61pPWCCtnk9+XD5iNoohQosvgMBH57NWZC1yCzBQ=="), /*93*/
					decoder.decode("BRdpFsFHX5FS/oTm9lACp4QJ5ApYHYUFmN6KFclRp6ePjyhy5yTcmEae3RkTOJWF4pvGDu4ii82dNnWMYFyD+w=="), /*94*/
					decoder.decode("BDm+YMAB+p/WY4nzegLF7zzrzzd28bmHuR2uzYiQTfzJ6oh8WE4/xD5zYBWAl63j9yhwe34JXGkE2tt0AJJx6Q=="), /*95*/
					decoder.decode("UeBBVN3mjDECCEJQisFRqIrkAIrpCaFmybGhXDEruy0PCW0MrXA0b0peMAzNKVG+uGw+O+oCBcfkyx2lby9Rog=="), /*96*/
					decoder.decode("t6bOibOlb+yLYlAclIU1tAwJNBcDWRq+1NJYaHcfIFBdXpxe3VD4U4DpyoIvKzdV9bqSSAi8H6Sf0chRk213Tg=="), /*97*/
					decoder.decode("mKF8IxWv4sEXZNunJECYrQju/K0Yq377Dh64Mr4gVTGY2Bz4QwlyEjUflNrA4sW99UKr0esIxwbktk/mz3FPgw=="), /*98*/
					decoder.decode("ru9rfMMNiaQJ9J09hZI+2YAwh4t3p5RdizTnCzxXZ6xb8zpZIQXTE/LAomXLBW/7WO/psUatNvhkORUaBqPEfw=="), /*99*/
					decoder.decode("hHwJO4e59OceUHQJC1otLZjV/TQRmCIhq+Fn6YRuVwLcl/riUWUU07M//bbX4/AEsxFIK5O8bggR/wEEXsBWpA=="), /*9A*/
					decoder.decode("ReohTYBfdQnCx/OyBWncEZALW/UVMHLrswhawFjpmT82Cwx29ozZlmv4khPHcw9VpFEb4R1J74UAKJbYyH7yow=="), /*9B*/
					decoder.decode("VS+rTq+4CFYMIhINuquRKvSFDyJ3ylOTxlyI/KVgH9DHSrLp2SZpghmoa/RgBe9SqV5YFIt//sdwkOWyzhC3ig=="), /*9C*/
					decoder.decode("KWt3ws1/zn/IBCi7XLElvxBqumHhwP+iuSgCeqij5iI8k1tI6z6SL2UG2BJmrIaOqOwlqzc7v/hSTnB5yRVvvA=="), /*9D*/
					decoder.decode("SqhAqSuLB2q61S56GwbTOSgqqCExLbjbjr8lHqptd33m80YLFIYsKruQjwPmEmWU80wW622xXnzA+j5waoWhHQ=="), /*9E*/
					decoder.decode("9kYJQ5RMCVrchs+5sClXfnd+WtTvvmSOPuDv6f/emXgSJjGLRy40fhxwuUJ/OcDaNxRiK2zrw6jhrsHNxb7ZKg=="), /*9F*/
					decoder.decode("hBBtjI0ITJsVKsX6oXLamf15PJ8YDo04dWgBj5jjym/6DV/1ihvUeeW5YKhh1juzKgLG1iNbJETj5MGFxl5wwA=="), /*A0*/
					decoder.decode("IEmP/8KIoCNRiG78KNPNAp5opvWzHj9LAcIrkXFVntPz7RBiHrraIKq7Khc5pYwLnfwVk06VXnBQKDnsxXKkwg=="), /*A1*/
					decoder.decode("kCsrbRNMhdLhdnb9lksoTEcS4dI06DSJUDdRT+5QfEOXvFOeOihGK0v1790YCZZkQ3mPZH6lD1kbMFPiPpuVkw=="), /*A2*/
					decoder.decode("opXBWiLNZWkctThiGBDABZJOBhyYXjSwYw52g+/I6yxuIgZsAbOTexwWpLHNyeRQ7Y4g/a5rYEpT2eKYsU7YKw=="), /*A3*/
					decoder.decode("2LzyccIVFUbqTUd1qr5HrzU0UrKRztd5WNAI0oToxqi5F3TSWJ5QCDMeYuw6yTcYcUCSoKPruTjp8GvddDuG5A=="), /*A4*/
					decoder.decode("6eFXtN+nicHCnIcq24T8/00VAST1EZo5KTAAOEJUg9QPnK2uC44EgNNZoeTTA8ishFHSZQCpPCXaXFBGCqc40A=="), /*A5*/
					decoder.decode("MyckwVE3zykUbCUmQpAL4DWaCW8l9gcxA2PGMyHo3GMAF51kG5iA7s/wBkevPYTLHDKPJ2Z5HB2FUQTk90DKQg=="), /*A6*/
					decoder.decode("aCoc6PZpHx4DGrsZ7CmqDgkuYyLyryQJKt2MmlcQdGUODTGFoFE3+NQ3ETp0JAp5nGhyIIdZylWY83IeVxsO+Q=="), /*A7*/
					decoder.decode("COHXMCwtEbVufWABahpscaTy+nSMFEnGyd2l92Lw1X9QdYSyk3sJmLVSXEhdsaCM4TE7Ae2oHW+nRMyXo0rHHQ=="), /*A8*/
					decoder.decode("EDQ0Ty1Qy6TsZATBlv9n7G645Uxg73qYlyrkOsRSwm6751MjtNcxObrgefR86msnLaXcPaklmcg0/qOMBE/ICw=="), /*A9*/
					decoder.decode("ZwvFstp1KA9LqUSc21ydMI74XhBPLGm1hkGaVgC5fo5nLkZ3kwTFt/2jkGmyOEKhdJT0/t2czQThTi1p2pBp5Q=="), /*AA*/
					decoder.decode("6rDoaHLShRbSCJSWaxJsy3hwjBdx1u3ETtTgq/ZARPZo7+nDNX8kwsotR1nuIh3BjJHFNZOKNshjS2ZFu++zHg=="), /*AB*/
					decoder.decode("4ChN64gLbJE+tGHJclOUgo/3U5Lpe5BOxerWLtrKSuUB9bzdlzjKYFVpRefqMS4Pu74ypfza4xOthzRYCpRy3A=="), /*AC*/
					decoder.decode("jyniFlv53zhmS3maA5PYSUp4wPD+Dx8p98OxuAZRLqPSR3cKEIm5+gF+5sNMA5kDLmEBLhSSwIOicNgUXve48A=="), /*AD*/
					decoder.decode("bQlucuDZHqk2GHcDCuhslIN5VDfs2aD5drFm/PwBfU3479zx2+N4D/HLo3olGHguQmMHGWiivxo/kDIv9X/6Mw=="), /*AE*/
					decoder.decode("FKlRpHtYS7viyTL9HAaXgvSX83txCBCeV9xh7VgYwhpMjfjNNHMq8ptVSRgi8p9loPOxgk0WhSZIv+O+xIlEfg=="), /*AF*/
					decoder.decode("vt/kTFQ0aHQX8WmI/FZMAD0Bx0fcCG3QKUgFe1Ffpp+ZVDrcYVswPCUFJBtp4Vvt7E9CNROI12M8zCDa3cPy3A=="), /*B0*/
					decoder.decode("p1i8lDXoarFBXlEUB79hcDFQbeAmLMoHzvlZ/gH2Qx1U6GnP2HofWWoQ2EuFd2qc3hGmSNS3a8yerXYq7LekTw=="), /*B1*/
					decoder.decode("T36KCSYVZS+8Jv6F8wzxEfk6irXXh8m+V/nH5JmNqCbh8t9rxa8CRIML4feGZwPCoGqDYRR+KNQNpQGH8tQtuw=="), /*B2*/
					decoder.decode("EoDEHJ7ZWB5Z7nyuim+ZFkbe8ewLtRzrN8Ek4PcyDcYTfpX5yDkf5ZTFoBXjBQ+pr4MD8hdjNb6VRada+NyFkw=="), /*B3*/
					decoder.decode("hwTIHrpeMQREj60HEFM1K/HPy6sm3X4o6VMV1nLlu8M5xUYPgTwwN/EbZ/4vnQxIjVMXBA1ppv159SJFLzlSEQ=="), /*B4*/
					decoder.decode("LZQzF9XIQ3cKyOs/pstcgSmztFQ8Iy03oeE6R+G+RRLVBkNY4jIH0n5p98VrQILkTe9+xeOe3VqSYKHIIbMIKQ=="), /*B5*/
					decoder.decode("wdF3YSqKV8J/UoNWmN6bY93jsrNE6RGrfav4/cf0H6FtXs0crplOrjT7p6i0kkG+KO//dVzUHL8CITONUmDguQ=="), /*B6*/
					decoder.decode("ETr13XrjbJP1ZodqU+f2Tfh5vE3AgnlF+DYcJLM0hThAUPf5WOHFED6mfACdAlNejGpm2T3ewBCi8SlIulyHmw=="), /*B7*/
					decoder.decode("qNj3zQGN/R71jX9DSZ541OixpMQ0X/Kya7AWMraWMCs6rnIcQvpzaxOxCa/CdYCdenqNE2s0juTrUtUHWPBndQ=="), /*B8*/
					decoder.decode("PoBky9R9DY7KERwHGLeJfWKlvGc+6ofmvn44RB3PGLbEyZ2ZxpVrLRvMBK2fd5Rmic6V3yw/f1O7CfdJ9O8PKw=="), /*B9*/
					decoder.decode("Az3M9JUZq3UJHhiLL0w6forYXbjJ/vOe67hnOBdu6cBux7O/sOoCDW3ZgOZoUMOjYQDokYv7H9nKB99Re1gAVA=="), /*BA*/
					decoder.decode("OvSb7RMmwZvd/BU15IjIIvFErbvTFno5OuUa9/UJ5R3retk4GPl/m/ILZgp3XGYN7JBPIs12VqsFa5FZyQH5mQ=="), /*BB*/
					decoder.decode("jfGAplh3jP4bk1TpsuygCBeXYtqy7acRTVBEyJlqLRAxeqVYYQp4uq/yTjKOiPubnz3M+2mzkurL13rjCqpwIQ=="), /*BC*/
					decoder.decode("fHlfnb/XpModrDQ1SZqdwCeQapVaU5lnlunzcTLaKS4nHjrJws7FsAA8B/t81uXe5HUKqWYLyD/+M2/Er+VeNw=="), /*BD*/
					decoder.decode("1DYto3RcJanGm7f7RWzHkrHOVdBiRX8+/btEKJOGPy3vSvYqHOkApYOpn61N+/xuQfDyDomLDKU0y5w8YeQU6w=="), /*BE*/
					decoder.decode("tpx3cK8mXqPSSPCBkt+sttInk3dXn7yJq/Ja251Qk8mIJSg93kuajYxkl4v5JrabyugoYq7MQy7ncLvEHNHaQA=="), /*BF*/
					decoder.decode("SsVr8gWZUJN3g3oanxa7ZjpnT0aajiKDN5E9b1nuMYy5KGeyL/Gi6qqtVhuMyMYSCoGPVhAhnSGktzz1zO+FAQ=="), /*C0*/
					decoder.decode("hVeDbINAEQOgx5ULS/s9gA2QVvO9ShjGx4Z193jGxSjc1J9/3bW743AFzEuQergz6yxtL9nF3uiz2RuwoRf7Cg=="), /*C1*/
					decoder.decode("RJ91bxAA7jCSDEBzwT1P+Pl6B9vGva4SZx3P39OXJidYVirTLm+5v6BnG8Ao1LVQ/KuT6CIgPJGCLm7oX8ytAQ=="), /*C2*/
					decoder.decode("4q+A40mpqOsr93nVxvR/cM+Z6KDIkXPPPSY7A3tAQwVcuPnIcL28IcTYjyBawSmhzlSPWUmZGbcyXq1P8Z/zFg=="), /*C3*/
					decoder.decode("wfgwzsvB/GrH2fjiux4t839ciowt9l6XwHYsdIattyieXtfeu/bR5PkdW1IVjRhnnxKSyL8bRq3SG/L6PJoHOg=="), /*C4*/
					decoder.decode("8+A6kHyKbBxXfVDsdarZ8nN+rm2AeMXcrhecSSRhGU7+v7oeM3CLtnjEITnoZc8JBv0xkNQQ7ukE1xiH6GCKnw=="), /*C5*/
					decoder.decode("ErO0BLoWU3IWUwaf6RvNHJ96Utq61fhz3vT7XyjOueFJr6B6xxPHRklPIt7kT1I7szvST+yessbcMu7SuK+iaw=="), /*C6*/
					decoder.decode("pQrTecK9e+79xd8ahkMdHD75psAqguIAmJVA2q3BRAsGGWe6kwS/4ggZODEI7Sl6v/w2j+uOYg58tQNZWepJ3g=="), /*C7*/
					decoder.decode("hkTKdOYXVa3qJiqmOTFVQoHPnIJ72rOPMah1bmqJDFnnegLxn1o/raD8QqUWeCqCx8OFh8de/06HD0oPBKPhFA=="), /*C8*/
					decoder.decode("sHHmxDjrmVK71cuuO70JFxxOK3kBqhr5KLBL7xHIz0Rn+ZjbBA7CdwY+HLhYKE6+ROEMGhVgilKEq6Te9LHQbQ=="), /*C9*/
					decoder.decode("gi0j0HwioGjYyt5cJS8bnZeVUAISv0p2ERGdbV0iHnCkFSMno3RdpMgwNYXxfveRXmMAdt2tPi4rhO5FpB/Riw=="), /*CA*/
					decoder.decode("rNnVcwFwNntS6YJgjySS/Z4bOG1d4gg2cbmQuwhfSU3KUvapWzSYLcQ3wtd5+6AvnskUxeD2qaSVuq9QcijjTQ=="), /*CB*/
					decoder.decode("d+ztO4jDjy3BsDlPkn/HV3Dyq9zlgT7bVzDc20jFPTOxI5rQc53+IORc9J8CA3jZMthkEtknw0GMg2hKPW0HFg=="), /*CC*/
					decoder.decode("1gEamd0bOBIoDmcA72PZclJ+oDPRmE/jiHsGZP7Hj5VdhVIctOoptTx4gOAqLdCHsMPePnuMnRgvPCQ4VJefDQ=="), /*CD*/
					decoder.decode("krTdPOr0Bd24FZ2CuBty9gbN8AuGftt6OCaTPSd5wDOhtSUGWqHugu/Iz5K0V8SVqgbKXCONJAMgOs8P2dB6PA=="), /*CE*/
					decoder.decode("Bhsg/uIV5yoIFDGeBU4Vom2AChnueWtfHfO9jq2yBm4QT30sR0AHlj3rYRlSITAexJ0JzwKXsJWEC1XWGkMdiA=="), /*CF*/
					decoder.decode("jmL2kYpEkl+/i9hWLA6O++iUm0vWCUTdmKVJjjt8vIptoAsf24SwHQmG8FfzG3on7wjbVJljV1MjhnayJr/KzQ=="), /*D0*/
					decoder.decode("OkNlDWlpZ2stU2rd9eh1v15X1MVKJ5kVDzPruC0hMjlubkW+Rct4q+wd+127xTxW6RQfIPgN132GfBDa1nPY7Q=="), /*D1*/
					decoder.decode("Yl7vfxsdZ6wzFbfBa4nPBwNGJccgs8sRm+U4N8sI5zLT1k6M7EtJxeRuQyHfR5ETiATo9pjnKX4XqtoAFRwUfQ=="), /*D2*/
					decoder.decode("U07Mximuq3PWisze/ssIP0NB7+MaLjW138Pca8IIM0VhXsnt8/QBTl0jZZgLrrjZU6ACgF9I5Mo7HkuxSux4bQ=="), /*D3*/
					decoder.decode("gk71IEiFUHSlrwax+08fW9/8izqjF0LyQ77KqQCS6p5hkH6KdCrHg61P9Ms0NzRZmPUtl8PRd/F5zLQymXkxKQ=="), /*D4*/
					decoder.decode("A3tZXIQ2iQGwO/kugVZqSG6N0dPEQ+nN7mVpr9c7KcheeIfSDdItWW1aGj4sryxnEdIr23k8wmFabB5JjSdEkQ=="), /*D5*/
					decoder.decode("tbamslzCgTWZ5sIfZZKG4N5AHGh2jmJZRC5l5Do8hV+b9uPkkvXy5wVAwxJw0VBcmNNdWHIS/8bFlz2GvSPgEg=="), /*D6*/
					decoder.decode("TxraJn5jVc1HE/HseyAHQMCXPPjdvAAabIPNdE2ffN4c8Zqu7FfBAtLXj1G0dcfHydgu9wJHjfmRwkfqgAryUQ=="), /*D7*/
					decoder.decode("P6vYTn4G4dSuEc9ZTl6ERRkUACsw09dNN57aBi2ezqH1RtQ0D+apvN1iEBInZut6/2koh/B2UG1gh3FoHWRw0w=="), /*D8*/
					decoder.decode("3/QncI1R8cpfY4KCaZ29Nkx21EaU0pNbC8d0meh9jL0qC+rSjfjQDXHrmn/Iu944bcJonILE5dFXalUqZUsbFw=="), /*D9*/
					decoder.decode("dM+rWWxsUuQZzbW9TJtoycCfmjB5zJ81LH9+SzJe83rqFyTCAj/77937zC0UdlO43Dx8XL/X2xH0kT+w2uSj7g=="), /*DA*/
					decoder.decode("Z6xuTapS8BIvYKZwcX/TEb8rQ2oiqmZtKX5ahwUSvYKK7vU5/G1v+emDuWYKuFw4wh9H7CyZ7WHGgvTG4cHQNg=="), /*DB*/
					decoder.decode("3QKYUC7QeMVyoz4XzrYJnBch9z1t/VAbz0eBqQUZqvZ1Mw3GAai82GysvpMVDO7m7UuM7eyAbR1VgoF0rLGo9g=="), /*DC*/
					decoder.decode("KpTs/rj674f3bqfH9+EKYDy0YDLsstpTfyhhLdDkPBZTbM4yR6H7s8Z8k9/XzWvmXFfRgKcKTQirBv//GxqaiA=="), /*DD*/
					decoder.decode("usdl01a08iaFRhX2JE697i6lh8AphID4dLp9TlSioIj/Bt5lnsvqnkc0meI2EKQQybXCo8hCfmiXMpTL65Am7Q=="), /*DE*/
					decoder.decode("WvtB1Ra8b07T5K5FEXDguUEEGHahks74uFsrdm78x3j4BLANvq3nndj/QHrWm3IOHsa/nWPAJgI/w0w3jXJzLA=="), /*DF*/
					decoder.decode("vdFCF9vu9hE6BaA2eTnozUdr1IWVHkzJUlbMAlggfvR3vRA16rRk6DerJuRJOuFjHkcR/JDyBZ41TF+DcDwXpw=="), /*E0*/
					decoder.decode("o4WVFn+B3ngCevWwEJwdMEttDsgOWpt1YU2Cm8w9cFuA0oI9ilsvStNACbdsCHIZcAMaFbdkyP/PjAtTP7AVeg=="), /*E1*/
					decoder.decode("WoI4sookvU0lvz0JCcvCrZtwb8GKIQ+GfMWImE0eg2XdruSmf3c/Y7qUpKQ9JTv/MMnsziCe11aAHKSuIFXl1w=="), /*E2*/
					decoder.decode("WbFetHp9kzIMk+sTaqmNI6J20f2Keqp1Rwj4sOuAN8htGT3JOsbzioKMaqOrAYAsK5xkHWPYsVIvKUTxKuCT9A=="), /*E3*/
					decoder.decode("xeJJX0TFkQh22mpaOXMgy98UJiixsXMRKDfKHYFIWCZg6fZ22tmURQYjistua6zRipd7bga0cEZeRAixLInV0g=="), /*E4*/
					decoder.decode("VM9N5f48ejewY/sAi43ccEb2rh8oPUF+7TBXQXx4pONf1P/jwpTKw0iU1hhUlufVUm5zYPz40j0XDW4+HagI9Q=="), /*E5*/
					decoder.decode("WO9zqW57jAj2u4NURvjS/qTSFWg7tJI07uj+7Nvg60LS2+eeijz+cvUu7rKBlj1dHMGiADgX0pqc/OIxqW5s0A=="), /*E6*/
					decoder.decode("5r493M5PGXefvu3Li3B0BvaycHCRNpD6v5tb5IKQAJU7zmVauwvIwem+rHk3vYjQg3xsR3RaCHwJX7clh4uanw=="), /*E7*/
					decoder.decode("dY26RyPio+ixpBfloWXP7Fop0oRektjyhu+Y2dU8f5qJk2V7SX2gnjUGt9cfFXoqF3P5VR/SM0ymz/AUnspyXA=="), /*E8*/
					decoder.decode("98ymvv87j5SOzCfE7MyMKQG9/1NWBRA28+JsU6ZZ2nrmw4paMDmxILCQ3vrIXLZrYzwUfVBGNz7BoO4a0RuRkA=="), /*E9*/
					decoder.decode("d+Zaej1Bim69aT6CDlIC0yTMqVrzecG2kE/TFPWs4RAbDOZbJlhTbmCJUOIjHn76PMjY9UQM+hiR1hGTR8Aa8g=="), /*EA*/
					decoder.decode("CHGH0z8isDdNXqeaK2KZn08Ccpy/P/0m/o3C+0qT/FyHe824D5guZx+YwaL3yqAjE8unsqhW3EFsYTg+TbT+dg=="), /*EB*/
					decoder.decode("qrL5+abDzuZrERjcWQH1uwEj5+JEGrvHaHYhyXA73saHMnX/pGtanPqufLtP9bETVqWA/aBVneAg+EGdKYVvgQ=="), /*EC*/
					decoder.decode("dg8D0LETXnAPJ9F1+WumkpW7Dsx9xk/IvqqFCpXsZEB5c9W4NpQ86JQBBLFAxUQmpnmRnnwm/CATxPQ7C9A4Yw=="), /*ED*/
					decoder.decode("anJvpx5jswF0mYnwFVTrY06lk1/W264uwouVfK7z5miiVNbG+BGaiYXG0W/qpDX0/bh/q4lcdoqIxa0/d4uzoQ=="), /*EE*/
					decoder.decode("i5OQr0ytB4zyaqeEyy1g13946wCICgxtBgwnXjGs3kOBHrHK7xV3k0v/noZI/vqfQ1vZKAo2I+10hbg243GIEw=="), /*EF*/
					decoder.decode("p2/Gg2MfCeCr279AfjwyBOF5PqbH477wCzzlX7t17k+GzhCD4rGAbQx6o+2u4o4Ya6FvrPazjReqXNAyA3W1qQ=="), /*F0*/
					decoder.decode("cZBFMEVOB0bntS2vHI9fSO9igj9X21JK/IWrvrw46OC8iUFqi0U9anNhaOcxPuiD4q+ImEtSUBD/HfxBnxKrEQ=="), /*F1*/
					decoder.decode("2zO2ZmkVFFSz5d0pSQxZvBajobwCMDV2RJFDAscglRfuL2R/GRLFn3ItWwOk7/vQrtaPnoJ/8vyemzo4vRmabg=="), /*F2*/
					decoder.decode("/vU0iNcaMrxotJLKUPCLqe3g6YbSNbU5Gw1Sp55rh+KZ1MzxN8PjfAjtnsd04Zlz7m1FfdWPSHb7AbEXOi5JNA=="), /*F3*/
					decoder.decode("E+I0ZGSSqlAvjlei7o3ytXIIR9i+45nJTssB64n7rQjSOPzzLrMCORSZa62WH/WJDQVvDPiLFE4UTAnRdIj2yg=="), /*F4*/
					decoder.decode("R84sk6W/hXdFkmHNwaa5ME1XzrbEA62pdMRjtg9Q19w/V1a1V1wpEd6PpoQBqpfgKOcNjZoTEjrR5wj9P5KEuA=="), /*F5*/
					decoder.decode("ni5jVfFLV3Z/ujxN/+cIer21gyObUE6tZ2mMRDSg97rDINvYRZ1JdlB1OBN4qmwDLVyPVzLoOWTPa3lmoS1H4A=="), /*F6*/
					decoder.decode("3uJAd+GVgtfAstwNYcp9rxauUReueFIoUm/neTKbc2ankRm3H8Cd6H72ESyaFAdk16RVDqKU0wz61OdqMDLADg=="), /*F7*/
					decoder.decode("ziN0gc5GehbTVOXLh4WpUuvne4uRjZxKYGfwikm8UGx+AalmPHy21ZjzJzUIkYwkUg6hu0zflI7i88DnN+6jEw=="), /*F8*/
					decoder.decode("sDnnEZS23y8tmffY37dlXDQq6K5G5Uqq2oSVqazM4zXeWzWVfOAi5WuVNlCpi5nR47rSFVAIS/TKMzHndq3sUQ=="), /*F9*/
					decoder.decode("RWwewNav/ncQew2Q5rKdtz0NtPKHHdbA7PJGi7/t1OmvNgGALj7nFefTRXM4tn35CR7I6nK8Q3IcqatxcUwWFw=="), /*FA*/
					decoder.decode("ohkyqSzbKsFQlVYa6u8CVqNGFQ06st/yhb6ZsuKXrHJMgqyz/b3nzCppyRu9oFvJMOIX2iDjUdWHFlRf3WXQ0g=="), /*FB*/
					decoder.decode("8mJyWjrMR4EeUQfbyASEjcLBNNAnVmQwUV6CYQ02X+lwFSGjQlkXl4lM0WCLiUTxDDhZ2FMx0u4+fo9LSEmpxA=="), /*FC*/
					decoder.decode("lBTK96TPLmbOMGYiFWick+0itHk+VVWrwvDeqvFTPrYMtpfL1QhSIVYyX9yRG/L3kUYMGor5sTuCAiutwpyhFw=="), /*FD*/
					decoder.decode("6Ft2DOvDF26R5TgTJKcQBN1jBBCV391JgToC9DXf9v2gqdKvX3Da3CbcFIckkqZzKYGTeUmX+YsI/PGBl6FOlQ=="), /*FE*/
					decoder.decode("kexwzlmzGURAWP2H6yBEMvZAzwsCDWndwUsaeovBfRjmAzfBz2hcNThbghX4cAQ67/evc9YxSKi+MJSEkgG7eA==")  /*FF*/
				}
			};
		}
	}

	/* =================================================================== */
	/* HELPER CLASSES                                                      */
	/* =================================================================== */

	/**
	 * This class provides a minimal wrapper around the "raw" HMAC-SHA512 key
	 */
	private static class HmacSha512Key implements SecretKey {
		private static final long serialVersionUID = 5663422355419849960L;
		private byte[] key;
		private boolean destroyed = false;
		
		public HmacSha512Key(final byte[] key) {
			if(isNullOrEmpty(key)) {
				throw new IllegalArgumentException("Key must not be null or empty!");
			}
			this.key = key; /*intentionally do *not* clone() here*/
		}
		
		@Override
		public String getAlgorithm() {
			return HMAC_SHA512;
		}
		
		@Override
		public String getFormat() {
			return "RAW";
		}
		
		@Override
		public byte[] getEncoded() {
			if (destroyed) {
				throw new IllegalStateException("This key is no longer valid!");
			}
			return this.key; /*intentionally do *not* clone() here*/
		}
		
		@Override
		public void destroy() throws DestroyFailedException {
			destroyed = true;
			Arrays.fill(this.key, (byte)0);
		}
		
		@Override
		public boolean isDestroyed() {
			return destroyed;
		}
		
		@Override
		public int hashCode() {
			if (destroyed) {
				throw new IllegalStateException("This key is no longer valid!");
			}
			return Arrays.hashCode(this.key);
		}

		@Override
		public boolean equals(final Object other) {
			if (destroyed) {
				throw new IllegalStateException("This key is no longer valid!");
			}
			if(other instanceof HmacSha512Key) {
				final HmacSha512Key otherKey = (HmacSha512Key) other;
				if (otherKey.destroyed) {
					throw new IllegalStateException("Other key is no longer valid!");
				}
				return Arrays.equals(this.key, otherKey.key);
			}
			return false;
		}
	}
	/**
	 * This class provides a ByteArrayOutputStream which is compressed by the DEFLATE algorithm
	 */
	private static class CompressedOutputStream extends DeflaterOutputStream {
		public CompressedOutputStream() {
			super(new ByteArrayOutputStream(), new Deflater(9, true), true);
		}
		
		@Override
		public void write(final int value) throws IOException {
			write(intToBytes(value));
		}

		public void write(final long value) throws IOException {
			write(longToBytes(value));
		}

		public byte[] toByteArray() throws IOException {
			close(); /*force Deflater.finish()*/
			return ((ByteArrayOutputStream)out).toByteArray();
		}
	}
}
